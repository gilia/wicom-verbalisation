<?php
/*

   Copyright 2018 GILIA

   Author: GILIA

   grammarconnectortest.php

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//require_once("common.php");
require_once('D:\xampp\htdocs\wicom-verbalisation\tests\php\common.php');


// use function \load;
//load("config.php", "config/");
load("config.php", "D:/xampp\htdocs\wicom-verbalisation\web-src/config/");
//load("grammarconnector.php", "wicom/reasoner/");
load("grammarconnector.php", "D:/xampp\htdocs\wicom-verbalisation\web-src/wicom/reasoner/");
use Wicom\Reasoner\GrammarConnector;

class GrammarConnectorTest extends PHPUnit\Framework\TestCase
{

    public function testReasoner(){
        $input = <<< EOT
{
    "words": [
        {
            "name": "Persona",
            "grammar": {
                "gender": "",
                "number": "",
                "plural": "",
                "syllabes": "",
                "capitalise": "",
                "article": [],
                "preposition": []
            }},
            {
            "name":                    "Carrera",
            "grammar": {
                "gender": "",
                				"number": "",
                "plural": "",
                "syllabes": "",
                "capitalise": "",
                "article": [],
                "preposition": []
            }
        },
    {
            "name":                    "Alumno",
            "grammar": {
                "gender": "",
                				"number": "",
                "plural": "",
                "syllabes": "",
                "capitalise": "",
                "article": [],
                "preposition": []
            }
        }]
}
EOT;

        $expected = <<< EOT
{"words":[
    {"word":"Persona",
            "grammar":{"gender":"femenino",
                        "number":"singular",
                        "plural":"Personas",
                        "syllabes":"per-so-na",
                        "capitalise":"Persona",
                        "article":["la Persona","las Personas","una Persona","unas Personas"],
                        "preposition":false}},
        {"word":"Carrera",
            "grammar":{"gender":"femenino",
                        "number":"singular",
                        "plural":"Carreras",
                        "syllabes":"ca-rre-ra",
                        "capitalise":"Carrera",
                        "article":["la Carrera","las Carreras","una Carrera","unas Carreras"],
                        "preposition":false}},
        {"word":"Alumno",
            "grammar":{"gender":"masculino",
                       "number":"singular",
                       "plural":"Alumnos",
                       "syllabes":"a-lum-no",
                       "capitalise":"Alumno",
                       "article":["el Alumno","los Alumnos","un Alumno","unos Alumnos"],
                       "preposition":false}}
        ]
}
EOT;

        $crowdgrammar = new GrammarConnector();

        $crowdgrammar->run($input);
        $actual = $crowdgrammar->get_col_answers()[0];
        
        
        //print_r ("Gramática devuelta".$actual);
        $order  = array("/\r\n/", "/\n/", "/\r/","/\s+\s+/");
        $replace = '';
        
        $expected1=preg_replace($order,$replace,$expected);
        
        $this->assertEquals($expected1, $actual, true);
    }
}
