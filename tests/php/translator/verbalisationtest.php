<?php
/*

Copyright 2017

Grupo de Investigación en Lenguajes e Inteligencia Artificial (GILIA) -
Facultad de Informática
Universidad Nacional del Comahue

foltest.php

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#require_once("common.php");
require_once('D:\xampp\htdocs\wicom-verbalisation\tests\php\common.php');

// use function \load;
#load("umlfol.php", "wicom/translator/fol/");
#load("verbalisationUML.php", "wicom/translator/verbalisation/");
load("umlfol.php", "D:/xampp\htdocs\wicom-verbalisation\web-src/wicom/translator/fol/");
load("verbalisationUML.php", "D:/xampp\htdocs\wicom-verbalisation\web-src/wicom/translator/verbalisation/");
load ("postagger.php","D:/xampp\htdocs\wicom-verbalisation\web-src\wicom/translator/verbalisation/");
load ("grammarconnector.php","D:/xampp\htdocs\wicom-verbalisation\web-src\wicom/reasoner/");
load ("verb_conjugator.php","D:/xampp\htdocs\wicom-verbalisation\web-src\wicom/translator/verbalisation/");

use Wicom\Translator\Fol\UMLFol;
use Wicom\Translator\Verbalisation\Verbalisation;
use Wicom\Translator\Verbalisation\PosTagger;
use Wicom\Reasoner\GrammarConnector;
use Wicom\Translator\Verbalisation\VerbConjugator;


class FolTest extends PHPUnit\Framework\TestCase
//class FolTest extends PHPUnit_Framework_TestCase
 {

/*    public function testUMLFull() {
        $json = <<< EOT
{
    "classes": [{
            "name": "Persona",
            "attrs": [{
                    "name": "nombre",
                    "datatype": "String"
                }, {
                    "name": "apellido",
                    "datatype": "String"
                }, {
                    "name": "direccion",
                    "datatype": "String"
                }],
            "methods": [],
            "position": {
                "x": 582,
                "y": 95
            }
        }, {
            "name": "Cliente",
            "attrs": [{
                    "name": "id_Cliente",
                    "datatype": "String"
                }, {
                    "name": "fecha_Alta",
                    "datatype": "date"
                }],
            "methods": [],
            "position": {
                "x": 582,
                "y": 95
            }
        },{
            "name": "Profesor",
            "attrs": [{
                    "name": "id_Profesor",
                    "datatype": "String"
                }, {
                    "name": "titulo",
                    "datatype": "String"
                }],
            "methods": [],
            "position": {
                "x": 582,
                "y": 95
            }
        },{
            "name": "Alumno",
            "attrs": [{
                    "name": "legajo",
                    "datatype": "String"
                }, {
                    "name": "fecha_Ingreso",
                    "datatype": "date"
                }],
            "methods": [],
            "position": {
                "x": 582,
                "y": 501
            }
        },{
            "name": "Carrera",
            "attrs": [{
                    "name": "id_Carrera",
                    "datatype": "String"
                }, {
                    "name": "nombreCompleto",
                    "datatype": "String"
                }],
            "methods": [],
            "position": {
                "x": 600,
                "y": 602
            }
        }, {
            "name": "Phone",
            "attrs": [{
                    "name": "id_Phone",
                    "datatype": "Integer"
                }],
            "methods": []
        },
        {
            "name": "CellPhone",
            "attrs": [{
                    "name": "id_CellPhone",
                    "datatype": "String"
                }, {
                    "name": "nombre",
                    "datatype": "String"
                }],
            "methods": []
        },
        {
            "name": "FixedPhone",
            "attrs": [{
                    "name": "id_FixedPhone",
                    "datatype": "String"
                }, {
                    "name": "date",
                    "datatype": "Date"
                }],
            "methods": []
        },
        {
            "name": "Phone_Call",
            "attrs": [{
                    "name": "id_PhoneCall",
                   "datatype": "String"
                }, {
                    "name": "type",
                    "datatype": "String"
                }],
            "methods": []
        }],
    "links": [{
            "name": "estudia",
            "classes": ["Persona", "Carrera"],
            "multiplicity": ["1..4", "1..1"],
            "roles": [null, null],
            "type": "association"
        },{
            "name": "call",
            "classes": ["Phone", "Phone_Call"],
            "multiplicity": ["1..*", "1..6"],
            "roles": [null, null],
            "type": "association"
        },
        {"classes" : ["CellPhone", "FixedPhone"],
			 "multiplicity" : null,
			 "name" : "r1",
			 "type" : "generalization",
			 "parent" : "Phone",
			 "constraint" : ["disjoint","covering"]
			},
        {"classes" : ["Cliente", "Profesor", "Alumno"],
			 "multiplicity" : null,
			 "name" : "r2",
			 "type" : "generalization",
			 "parent" : "Persona",
			 "constraint" : ["covering"]
			}
    ]
}
EOT;

        $expected = <<< EOT
{
    "Classes": [{
            "forall": {
                "var": "x",
                "pred": {
                    "name": "Persona",
                    "varp": ["x"]
                }
            }
        }, {
            "forall": {
                "var": "x",
                "pred": {
                    "name": "Cliente",
                    "varp": ["x"]
                }
            }
        }, {
            "forall": {
                "var": "x",
                "pred": {
                    "name": "Profesor",
                    "varp": ["x"]
                }
            }
        }, {
            "forall": {
                "var": "x",
                "pred": {
                    "name": "Alumno",
                    "varp": ["x"]
                }
            }
        },{
            "forall": {
                "var": "x",
                "pred": {
                    "name": "Carrera",
                    "varp": ["x"]
                }
            }
        }, {
            "forall": {
                "var": "x",
                "pred": {
                    "name": "Phone",
                    "varp": ["x"]
                }
            }
        }, {
            "forall": {
                "var": "x",
                "pred": {
                    "name": "CellPhone",
                    "varp": ["x"]
                }
            }
        }, {
            "forall": {
                "var": "x",
                "pred": {
                    "name": "FixedPhone",
                    "varp": ["x"]
                }
            }
        }, {
            "forall": {
                "var": "x",
                "pred": {
                    "name": "Phone_Call",
                    "varp": ["x"]
                }
            }
        }],
    "Attribute": [{
            "forall": {
                "var": ["x", "y"],
                "imply": {
                    "and": {
                        "pred": {
                            "name": "Persona",
                            "varp": ["x"]
                        },
                        "predA": {
                            "name": "nombre",
                            "varp": ["x", "y"]
                        }
                    },
                    "predT": {
                        "name": "String",
                        "varp": ["y"]
                    }
                }
            }
        }, {
            "forall": {
                "var": ["x", "y"],
                "imply": {
                    "and": {
                        "pred": {
                            "name": "Persona",
                            "varp": ["x"]
                        },
                        "predA": {
                            "name": "apellido",
                            "varp": ["x", "y"]
                        }
                    },
                    "predT": {
                        "name": "String",
                        "varp": ["y"]
                    }
                }
            }
        }, {
            "forall": {
                "var": ["x", "y"],
                "imply": {
                    "and": {
                        "pred": {
                            "name": "Persona",
                            "varp": ["x"]
                        },
                        "predA": {
                            "name": "direccion",
                            "varp": ["x", "y"]
                        }
                    },
                    "predT": {
                        "name": "String",
                        "varp": ["y"]
                    }
                }
            }
        }, {
            "forall": {
                "var": ["x", "y"],
                "imply": {
                    "and": {
                        "pred": {
                            "name": "Cliente",
                            "varp": ["x"]
                        },
                        "predA": {
                            "name": "id_Cliente",
                            "varp": ["x", "y"]
                        }
                    },
                    "predT": {
                        "name": "String",
                        "varp": ["y"]
                    }
                }
            }
        }, {
            "forall": {
                "var": ["x", "y"],
                "imply": {
                    "and": {
                        "pred": {
                            "name": "Cliente",
                            "varp": ["x"]
                        },
                        "predA": {
                            "name": "fecha_Alta",
                            "varp": ["x", "y"]
                        }
                    },
                    "predT": {
                        "name": "date",
                        "varp": ["y"]
                    }
                }
            }
        }, {
            "forall": {
                "var": ["x", "y"],
                "imply": {
                    "and": {
                        "pred": {
                            "name": "Profesor",
                            "varp": ["x"]
                        },
                        "predA": {
                            "name": "id_Profesor",
                            "varp": ["x", "y"]
                        }
                    },
                    "predT": {
                        "name": "String",
                        "varp": ["y"]
                    }
                }
            }
        }, {
            "forall": {
                "var": ["x", "y"],
                "imply": {
                    "and": {
                        "pred": {
                            "name": "Profesor",
                            "varp": ["x"]
                        },
                        "predA": {
                            "name": "titulo",
                            "varp": ["x", "y"]
                        }
                    },
                    "predT": {
                        "name": "String",
                        "varp": ["y"]
                    }
                }
            }
        }, {
            "forall": {
                "var": ["x", "y"],
                "imply": {
                    "and": {
                        "pred": {
                            "name": "Alumno",
                            "varp": ["x"]
                        },
                        "predA": {
                            "name": "legajo",
                            "varp": ["x", "y"]
                        }
                    },
                    "predT": {
                        "name": "String",
                        "varp": ["y"]
                    }
                }
            }
        }, {
            "forall": {
                "var": ["x", "y"],
                "imply": {
                    "and": {
                        "pred": {
                            "name": "Alumno",
                            "varp": ["x"]
                        },
                        "predA": {
                            "name": "fecha_Ingreso",
                            "varp": ["x", "y"]
                        }
                    },
                    "predT": {
                        "name": "date",
                        "varp": ["y"]
                    }
                }
            }
        }, {
            "forall": {
                "var": ["x", "y"],
                "imply": {
                    "and": {
                        "pred": {
                            "name": "Carrera",
                            "varp": ["x"]
                        },
                        "predA": {
                            "name": "id_Carrera",
                            "varp": ["x", "y"]
                        }
                    },
                    "predT": {
                        "name": "String",
                        "varp": ["y"]
                    }
                }
            }
        }, {
            "forall": {
                "var": ["x", "y"],
                "imply": {
                    "and": {
                        "pred": {
                            "name": "Carrera",
                            "varp": ["x"]
                        },
                        "predA": {
                            "name": "nombreCompleto",
                            "varp": ["x", "y"]
                        }
                    },
                    "predT": {
                        "name": "String",
                        "varp": ["y"]
                    }
                }
            }
        },{
            "forall": {
                "var": ["x", "y"],
                "imply": {
                    "and": {
                        "pred": {
                            "name": "Phone",
                            "varp": ["x"]
                        },
                        "predA": {
                            "name": "id_Phone",
                            "varp": ["x", "y"]
                        }
                    },
                    "predT": {
                        "name": "Integer",
                        "varp": ["y"]
                    }
                }
            }
        }, {
            "forall": {
                "var": ["x", "y"],
                "imply": {
                    "and": {
                        "pred": {
                            "name": "CellPhone",
                            "varp": ["x"]
                        },
                        "predA": {
                            "name": "id_CellPhone",
                            "varp": ["x", "y"]
                        }
                    },
                    "predT": {
                        "name": "String",
                        "varp": ["y"]
                    }
                }
            }
        }, {
            "forall": {
                "var": ["x", "y"],
                "imply": {
                    "and": {
                        "pred": {
                            "name": "CellPhone",
                            "varp": ["x"]
                        },
                        "predA": {
                            "name": "nombre",
                            "varp": ["x", "y"]
                        }
                    },
                    "predT": {
                        "name": "String",
                        "varp": ["y"]
                    }
                }
            }
        }, {
            "forall": {
                "var": ["x", "y"],
                "imply": {
                    "and": {
                        "pred": {
                            "name": "FixedPhone",
                            "varp": ["x"]
                        },
                        "predA": {
                            "name": "id_FixedPhone",
                            "varp": ["x", "y"]
                        }
                    },
                    "predT": {
                        "name": "String",
                        "varp": ["y"]
                    }
                }
            }
        }, {
            "forall": {
                "var": ["x", "y"],
                "imply": {
                    "and": {
                        "pred": {
                            "name": "FixedPhone",
                            "varp": ["x"]
                        },
                        "predA": {
                            "name": "date",
                            "varp": ["x", "y"]
                        }
                    },
                    "predT": {
                        "name": "Date",
                        "varp": ["y"]
                    }
                }
            }
        }, {
            "forall": {
                "var": ["x", "y"],
                "imply": {
                    "and": {
                        "pred": {
                            "name": "Phone_Call",
                            "varp": ["x"]
                        },
                        "predA": {
                            "name": "id_PhoneCall",
                            "varp": ["x", "y"]
                        }
                    },
                    "predT": {
                        "name": "String",
                        "varp": ["y"]
                    }
                }
            }
        }, {
            "forall": {
                "var": ["x", "y"],
                "imply": {
                    "and": {
                        "pred": {
                            "name": "Phone_Call",
                            "varp": ["x"]
                        },
                        "predA": {
                            "name": "type",
                            "varp": ["x", "y"]
                        }
                    },
                    "predT": {
                        "name": "String",
                        "varp": ["y"]
                    }
                }
            }
        }],
    "Links": [[{
                "forall": {
                    "var": ["x", "y"],
                    "imply": {
                        "pred": {
                            "name": "estudia",
                            "varp": ["x", "y"]
                        },
                        "and": {
                            "pred": {
                                "name": "Persona",
                                "varp": ["x"]
                            },
                            "predB": {
                                "name": "Carrera",
                                "varp": ["y"]
                            }
                        }
                    }
                }
            }, {
                "forall": {
                    "var": ["x"],
                    "imply": {
                        "pred": {
                            "name": "Persona",
                            "varp": ["x"]
                        },
                        "multiplicity": {
                            "min": "1",
                            "#": {
                                "var": ["y"],
                                "pred": {
                                    "name": "estudia",
                                    "varp": ["x", "y"]
                                }
                            },
                            "max": "*"
                        }
                    }
                }
            }, {
                "forall": {
                    "var": ["x"],
                    "imply": {
                        "pred": {
                            "name": "Carrera",
                            "varp": ["y"]
                        },
                        "multiplicity": {
                            "min": "1",
                            "#": {
                                "var": ["x"],
                                "pred": {
                                    "name": "estudia",
                                    "varp": ["x", "y"]
                                }
                            },
                            "max": "*"
                        }
                    }
                }
            }], [{
                "forall": {
                    "var": ["x", "y"],
                    "imply": {
                        "pred": {
                            "name": "call",
                            "varp": ["x", "y"]
                        },
                        "and": {
                            "pred": {
                                "name": "Phone",
                                "varp": ["x"]
                            },
                            "predB": {
                                "name": "Phone_Call",
                                "varp": ["y"]
                            }
                        }
                    }
                }
            }, {
                "forall": {
                    "var": ["x"],
                    "imply": {
                        "pred": {
                            "name": "Phone",
                            "varp": ["x"]
                        },
                        "multiplicity": {
                            "min": "1",
                            "#": {
                                "var": ["y"],
                                "pred": {
                                    "name": "call",
                                    "varp": ["x", "y"]
                                }
                            },
                            "max": "1"
                        }
                    }
                }
            }, {
                "forall": {
                    "var": ["x"],
                    "imply": {
                        "pred": {
                            "name": "Phone_Call",
                            "varp": ["y"]
                        },
                        "multiplicity": {
                            "min": "1",
                            "#": {
                                "var": ["x"],
                                "pred": {
                                    "name": "call",
                                    "varp": ["x", "y"]
                                }
                            },
                            "max": "*"
                        }
                    }
                }
            }]],
    "IsA": [{
            "forall": {
                "var": "x",
                "imply": {
                    "pred": {
                        "name": "CellPhone",
                        "varp": ["x"]
                    },
                    "predB": {
                        "name": "Phone",
                        "varp": ["x"]
                    }
                }
            }
        }, {
            "forall": {
                "var": "x",
                "imply": {
                    "pred": {
                        "name": "FixedPhone",
                        "varp": ["x"]
                    },
                    "predB": {
                        "name": "Phone",
                        "varp": ["x"]
                    }
                }
            }
        }, {
            "disjoint": [{
                    "forall": {
                        "var": ["x"],
                        "imply": {
                            "pred": {
                                "name": "CellPhone",
                                "varp": ["x"]
                            },
                            "neg": {
                                "pred": {
                                    "name": "FixedPhone",
                                    "varp": "x"
                                }
                            }
                        }
                    }
                }],
            "covering": [{
                    "forall": {
                        "var": ["x"],
                        "imply": {
                            "pred": {
                                "name": "Phone"
                            },
                            "or": [{
                                    "pred": {
                                        "name": "CellPhone",
                                        "var": ["x"]
                                    }
                                }, {
                                    "pred": {
                                        "name": "FixedPhone",
                                        "var": ["x"]
                                    }
                                }]
                        }
                    }
                }]
        }, {
            "forall": {
                "var": "x",
                "imply": {
                    "pred": {
                        "name": "Cliente",
                        "varp": ["x"]
                    },
                    "predB": {
                        "name": "Persona",
                        "varp": ["x"]
                    }
                }
            }
        }, {
            "forall": {
                "var": "x",
                "imply": {
                    "pred": {
                        "name": "Profesor",
                        "varp": ["x"]
                    },
                    "predB": {
                        "name": "Persona",
                        "varp": ["x"]
                    }
                }
            }
        }, {
            "forall": {
                "var": "x",
                "imply": {
                    "pred": {
                        "name": "Alumno",
                        "varp": ["x"]
                    },
                    "predB": {
                        "name": "Persona",
                        "varp": ["x"]
                    }
                }
            }
        }, {
            "covering": [{
                    "forall": {
                        "var": ["x"],
                        "imply": {
                            "pred": {
                                "name": "Persona"
                            },
                            "or": [{
                                    "pred": {
                                        "name": "Cliente",
                                        "var": ["x"]
                                    }
                                }, {
                                    "pred": {
                                        "name": "Profesor",
                                        "var": ["x"]
                                    }
                                }, {
                                    "pred": {
                                        "name": "Alumno",
                                        "var": ["x"]
                                    }
                                }]
                        }
                    }
                }]
        }]
}
EOT;

        $strategy = new UMLFol();
        $strategy->create_fol($json);
        
        $postagger = new PosTagger($json);
        $postagger->generate_SpGrammar();
        $empty_grammar= $postagger->get_SpGrammar();
        
        

        
        $crowdgrammar = new GrammarConnector();
        $crowdgrammar->run($empty_grammar);
        
        //$grammar = $crowdgrammar->get_col_answers()[0];
       
        $grammar = $crowdgrammar->get_json_grammar();
        
        $verbalisation = new Verbalisation();
        $verbalisation->generate_verbalisation($strategy->get_json(),$grammar);
        
        
        
        print_r($verbalisation->verbalisation);
        
        //print_r ($json_grammar);
    } 
    
    public function test_verbs() {
        $json = <<< EOT
{
    "classes": [{
            "name": "Persona",
            "attrs": [{
                    "name": "nombre",
                    "datatype": "String"
                }, {
                    "name": "apellido",
                    "datatype": "String"
                }, {
                    "name": "direccion",
                    "datatype": "String"
                }],
            "methods": [],
            "position": {
                "x": 582,
                "y": 95
            }
        }, {
            "name": "Cliente",
            "attrs": [{
                    "name": "id_Cliente",
                    "datatype": "String"
                }, {
                    "name": "fecha_Alta",
                    "datatype": "date"
                }],
            "methods": [],
            "position": {
                "x": 582,
                "y": 95
            }
        },{
            "name": "Profesor",
            "attrs": [{
                    "name": "id_Profesor",
                    "datatype": "String"
                }, {
                    "name": "titulo",
                    "datatype": "String"
                }],
            "methods": [],
            "position": {
                "x": 582,
                "y": 95
            }
        },{
            "name": "Alumno",
            "attrs": [{
                    "name": "legajo",
                    "datatype": "String"
                }, {
                    "name": "fecha_Ingreso",
                    "datatype": "date"
                }],
            "methods": [],
            "position": {
                "x": 582,
                "y": 501
            }
        },{
            "name": "Carrera",
            "attrs": [{
                    "name": "id_Carrera",
                    "datatype": "String"
                }, {
                    "name": "nombreCompleto",
                    "datatype": "String"
                }],
            "methods": [],
            "position": {
                "x": 600,
                "y": 602
            }
        }, {
            "name": "Telefono",
            "attrs": [{
                    "name": "id_Telefono",
                    "datatype": "Integer"
                }],
            "methods": []
        },
        {
            "name": "Telefono_Celular",
            "attrs": [{
                    "name": "id_Telefono_Celular",
                    "datatype": "String"
                }, {
                    "name": "nombre",
                    "datatype": "String"
                }],
            "methods": []
        },
        {
            "name": "Telefono_Fijo",
            "attrs": [{
                    "name": "id_Telefono_Fijo",
                    "datatype": "String"
                }, {
                    "name": "dia",
                    "datatype": "Date"
                }],
            "methods": []
        },
        {
            "name": "Llamada_Telefonica",
            "attrs": [{
                    "name": "id_PhoneCall",
                   "datatype": "String"
                }, {
                    "name": "type",
                    "datatype": "String"
                }],
            "methods": []
        }],
    "links": [{
            "name": "estudia",
            "classes": ["Persona", "Carrera"],
            "multiplicity": ["1..4", "1..1"],
            "roles": [null, null],
            "type": "association"
        },{
            "name": "realiza",
            "classes": ["Telefono", "Llamada_Telefonica"],
            "multiplicity": ["1..*", "1..6"],
            "roles": [null, null],
            "type": "association"
        },{
            "name": "tiene",
            "classes": ["Persona", "Telefono"],
            "multiplicity": ["1..*", "1..1"],
            "roles": [null, null],
            "type": "association"
        },{
            "name": "corre",
            "classes": ["Persona", "Carrera"],
            "multiplicity": ["1..*", "1..4"],
            "roles": [null, null],
            "type": "association"
        },{
            "name": "llama",
            "classes": ["Telefono_Celular", "Llamada_Telefonica"],
            "multiplicity": ["1..*", "1..6"],
            "roles": [null, null],
            "type": "association"
        },
        {"classes" : ["Telefono_Celular", "Telefono_Fijo"],
			 "multiplicity" : null,
			 "name" : "r1",
			 "type" : "generalization",
			 "parent" : "Telefono",
			 "constraint" : ["disjoint","covering"]
			},
        {"classes" : ["Cliente", "Profesor", "Alumno"],
			 "multiplicity" : null,
			 "name" : "r2",
			 "type" : "generalization",
			 "parent" : "Persona",
			 "constraint" : ["covering"]
			}
    ]
}
EOT;
        $strategy = new UMLFol();
        $strategy->create_fol($json);
        
        
        $postagger = new PosTagger($json);
        $postagger->generate_SpGrammar();
        $empty_grammar= $postagger->get_SpGrammar();
        $verbs=$postagger->get_grammar_for_links();
        
        
        $conjugator = new VerbConjugator($verbs);
        $conjugator->conjugate_verbs();
        $verbs_conj = $conjugator->get_conjugate_verbs();
        
        //print_r("asdfasd \n");
        //print_r(array_keys($verbs_conj[0])[0]);
        //print_r(array_values($verbs_conj[0])[0]); 
       
             
        $crowdgrammar = new GrammarConnector();
        $crowdgrammar->run($empty_grammar);
        
        $grammar = $crowdgrammar->get_json_grammar();
        
        $verbalisation = new Verbalisation();
        
        //$verbalisation->setVerbs($verbs_conj);
        
        $verbalisation->generate_verbalisation($strategy->get_json(),$grammar,$verbs_conj);
        
        print_r($verbalisation->verbalisation);

        
    } */

    public function mostrarResultados($json) {
        foreach ($json as $key => $value) {
            print($key + ':');
            print_r($value);
        }
    }

    public function verbalisation($fol) {
        $verbalisation = new Verbalisation();
        $verbalisation->generate_verbalisation($fol);
        print_r($verbalisation->verbalisation);
    }
    
    public function testFullUMLSpanish(){
        $json = <<< EOT
{
    "classes": [{
            "name": "Persona",
            "attrs": [{
                    "name": "nombre",
                    "datatype": "String"
                }, {
                    "name": "apeliido",
                    "datatype": "String"
                }, {
                    "name": "direccion",
                    "datatype": "String"
                }, {
                    "name": "DNI",
                    "datatype": "Integer"
                }, {
                    "name": "telefono",
                    "datatype": "String"
                }],
            "methods": [],
            "position": {
                "x": 313,
                "y": 21
            }
        }, {
            "name": "Cliente",
            "attrs": [{
                    "name": "id_cliente",
                    "datatype": "String"
                }, {
                    "name": "fecha_alta",
                    "datatype": "String"
                }],
            "methods": [],
            "position": {
                "x": 38,
                "y": 294
            }
        }, {
            "name": "Profesor",
            "attrs": [{
                    "name": "id_profesor",
                    "datatype": "String"
                }, {
                    "name": "titulo",
                    "datatype": "String"
                }],
            "methods": [],
            "position": {
                "x": 368,
                "y": 300
            }
        }, {
            "name": "Alumno",
            "attrs": [{
                    "name": "id_alumno",
                    "datatype": "String"
                }, {
                    "name": "legajo",
                    "datatype": "Integer"
                }, {
                    "name": "fecha_ingreso",
                    "datatype": "Date"
                }],
            "methods": [],
            "position": {
                "x": 640,
                "y": 290
            }
        }, {
            "name": "Universidad",
            "attrs": [{
                    "name": "nombre",
                    "datatype": "String"
                }, {
                    "name": "anio_creacion",
                    "datatype": "String"
                }],
            "methods": [],
            "position": {
                "x": 366,
                "y": 613
            }
        }, {
            "name": "Llamada_Telefonica",
            "attrs": [{
                    "name": "id_llamada",
                    "datatype": "String"
                }, {
                    "name": "tipo",
                    "datatype": "String"
                }, {
                    "name": "fecha",
                    "datatype": "Date"
                }],
            "methods": [],
            "position": {
                "x": 812,
                "y": 50
            }
        }, {
            "name": "Telefono",
            "attrs": [{
                    "name": "id_telefono",
                    "datatype": "String"
                }, {
                    "name": "numero",
                    "datatype": "Int"
                }, {
                    "name": "codigo_pais",
                    "datatype": "String"
                }],
            "methods": [],
            "position": {
                "x": 1282,
                "y": 55
            }
        }, {
            "name": "Telefono_Celular",
            "attrs": [{
                    "name": "modelo",
                    "datatype": "String"
                }, {
                    "name": "marca",
                    "datatype": "String"
                }, {
                    "name": "peso",
                    "datatype": "Double"
                }],
            "methods": [],
            "position": {
                "x": 1203,
                "y": 314
            }
        }, {
            "name": "Telefono_Fijo",
            "attrs": [{
                    "name": "anio_fabricacion",
                    "datatype": "Integer"
                }, {
                    "name": "tipo",
                    "datatype": "String"
                }],
            "methods": [],
            "position": {
                "x": 1436,
                "y": 308
            }
        }, {
            "name": "Carrera",
            "attrs": [{
                    "name": "id_carrera",
                    "datatype": "String"
                }, {
                    "name": "nombre",
                    "datatype": "String"
                }, {
                    "name": "anio_creacion",
                    "datatype": "Integer"
                }],
            "methods": [],
            "position": {
                "x": 643,
                "y": 612
            }
        }],
    "links": [{
            "name": "r1",
            "classes": ["Profesor", "Alumno", "Cliente"],
            "multiplicity": null,
            "roles": [null, null],
            "type": "generalization",
            "parent": "Persona",
            "constraint": ["covering"]
        }, {
            "name": "r2",
            "classes": ["Telefono_Celular", "Telefono_Fijo"],
            "multiplicity": null,
            "roles": [null, null],
            "type": "generalization",
            "parent": "Telefono",
            "constraint": ["disjoint", "covering"]
        }, {
            "name": "llama",
            "classes": ["Telefono", "Llamada_Telefonica"],
            "multiplicity": ["1..1", "1..*"],
            "roles": ["", ""],
            "type": "association"
        }, {
            "name": "estudia",
            "classes": ["Alumno", "Carrera"],
            "multiplicity": ["1..*", "1..*"],
            "roles": ["", ""],
            "type": "association"
        }, {
            "name": "trabaja",
            "classes": ["Profesor", "Universidad"],
            "multiplicity": ["1..*", "1..4"],
            "roles": ["", ""],
            "type": "association"
        }]
}
EOT;

        $expected = <<< EOT
{
    "Classes": [{
            "forall": {
                "var": "x",
                "pred": {
                    "name": "Persona",
                    "varp": ["x"]
                }
            }
        }, {
            "forall": {
                "var": "x",
                "pred": {
                    "name": "Cliente",
                    "varp": ["x"]
                }
            }
        }, {
            "forall": {
                "var": "x",
                "pred": {
                    "name": "Profesor",
                    "varp": ["x"]
                }
            }
        }, {
            "forall": {
                "var": "x",
                "pred": {
                    "name": "Alumno",
                    "varp": ["x"]
                }
            }
        }, {
            "forall": {
                "var": "x",
                "pred": {
                    "name": "Universidad",
                    "varp": ["x"]
                }
            }
        }, {
            "forall": {
                "var": "x",
                "pred": {
                    "name": "Llamada_Telefonica",
                    "varp": ["x"]
                }
            }
        }, {
            "forall": {
                "var": "x",
                "pred": {
                    "name": "Telefono",
                    "varp": ["x"]
                }
            }
        }, {
            "forall": {
                "var": "x",
                "pred": {
                    "name": "Telefono_Celular",
                    "varp": ["x"]
                }
            }
        }, {
            "forall": {
                "var": "x",
                "pred": {
                    "name": "Telefono_Fijo",
                    "varp": ["x"]
                }
            }
        }, {
            "forall": {
                "var": "x",
                "pred": {
                    "name": "Carrera",
                    "varp": ["x"]
                }
            }
        }],
    "Attribute": [{
            "forall": {
                "var": ["x", "y"],
                "imply": {
                    "and": {
                        "pred": {
                            "name": "Persona",
                            "varp": ["x"]
                        },
                        "predA": {
                            "name": "nombre",
                            "varp": ["x", "y"]
                        }
                    },
                    "predT": {
                        "name": "String",
                        "varp": ["y"]
                    }
                }
            }
        }, {
            "forall": {
                "var": ["x", "y"],
                "imply": {
                    "and": {
                        "pred": {
                            "name": "Persona",
                            "varp": ["x"]
                        },
                        "predA": {
                            "name": "apeliido",
                            "varp": ["x", "y"]
                        }
                    },
                    "predT": {
                        "name": "String",
                        "varp": ["y"]
                    }
                }
            }
        }, {
            "forall": {
                "var": ["x", "y"],
                "imply": {
                    "and": {
                        "pred": {
                            "name": "Persona",
                            "varp": ["x"]
                        },
                        "predA": {
                            "name": "direccion",
                            "varp": ["x", "y"]
                        }
                    },
                    "predT": {
                        "name": "String",
                        "varp": ["y"]
                    }
                }
            }
        }, {
            "forall": {
                "var": ["x", "y"],
                "imply": {
                    "and": {
                        "pred": {
                            "name": "Persona",
                            "varp": ["x"]
                        },
                        "predA": {
                            "name": "DNI",
                            "varp": ["x", "y"]
                        }
                    },
                    "predT": {
                        "name": "Integer",
                        "varp": ["y"]
                    }
                }
            }
        }, {
            "forall": {
                "var": ["x", "y"],
                "imply": {
                    "and": {
                        "pred": {
                            "name": "Persona",
                            "varp": ["x"]
                        },
                        "predA": {
                            "name": "telefono",
                            "varp": ["x", "y"]
                        }
                    },
                    "predT": {
                        "name": "String",
                        "varp": ["y"]
                    }
                }
            }
        }, {
            "forall": {
                "var": ["x", "y"],
                "imply": {
                    "and": {
                        "pred": {
                            "name": "Cliente",
                            "varp": ["x"]
                        },
                        "predA": {
                            "name": "id_cliente",
                            "varp": ["x", "y"]
                        }
                    },
                    "predT": {
                        "name": "String",
                        "varp": ["y"]
                    }
                }
            }
        }, {
            "forall": {
                "var": ["x", "y"],
                "imply": {
                    "and": {
                        "pred": {
                            "name": "Cliente",
                            "varp": ["x"]
                        },
                        "predA": {
                            "name": "fecha_alta",
                            "varp": ["x", "y"]
                        }
                    },
                    "predT": {
                        "name": "String",
                        "varp": ["y"]
                    }
                }
            }
        }, {
            "forall": {
                "var": ["x", "y"],
                "imply": {
                    "and": {
                        "pred": {
                            "name": "Profesor",
                            "varp": ["x"]
                        },
                        "predA": {
                            "name": "id_profesor",
                            "varp": ["x", "y"]
                        }
                    },
                    "predT": {
                        "name": "String",
                        "varp": ["y"]
                    }
                }
            }
        }, {
            "forall": {
                "var": ["x", "y"],
                "imply": {
                    "and": {
                        "pred": {
                            "name": "Profesor",
                            "varp": ["x"]
                        },
                        "predA": {
                            "name": "titulo",
                            "varp": ["x", "y"]
                        }
                    },
                    "predT": {
                        "name": "String",
                        "varp": ["y"]
                    }
                }
            }
        }, {
            "forall": {
                "var": ["x", "y"],
                "imply": {
                    "and": {
                        "pred": {
                            "name": "Alumno",
                            "varp": ["x"]
                        },
                        "predA": {
                            "name": "id_alumno",
                            "varp": ["x", "y"]
                        }
                    },
                    "predT": {
                        "name": "String",
                        "varp": ["y"]
                    }
                }
            }
        }, {
            "forall": {
                "var": ["x", "y"],
                "imply": {
                    "and": {
                        "pred": {
                            "name": "Alumno",
                            "varp": ["x"]
                        },
                        "predA": {
                            "name": "legajo",
                            "varp": ["x", "y"]
                        }
                    },
                    "predT": {
                        "name": "Integer",
                        "varp": ["y"]
                    }
                }
            }
        }, {
            "forall": {
                "var": ["x", "y"],
                "imply": {
                    "and": {
                        "pred": {
                            "name": "Alumno",
                            "varp": ["x"]
                        },
                        "predA": {
                            "name": "fecha_ingreso",
                            "varp": ["x", "y"]
                        }
                    },
                    "predT": {
                        "name": "Date",
                        "varp": ["y"]
                    }
                }
            }
        }, {
            "forall": {
                "var": ["x", "y"],
                "imply": {
                    "and": {
                        "pred": {
                            "name": "Universidad",
                            "varp": ["x"]
                        },
                        "predA": {
                            "name": "nombre",
                            "varp": ["x", "y"]
                        }
                    },
                    "predT": {
                        "name": "String",
                        "varp": ["y"]
                    }
                }
            }
        }, {
            "forall": {
                "var": ["x", "y"],
                "imply": {
                    "and": {
                        "pred": {
                            "name": "Universidad",
                            "varp": ["x"]
                        },
                        "predA": {
                            "name": "a\u00f1o_creacion",
                            "varp": ["x", "y"]
                        }
                    },
                    "predT": {
                        "name": "String",
                        "varp": ["y"]
                    }
                }
            }
        }, {
            "forall": {
                "var": ["x", "y"],
                "imply": {
                    "and": {
                        "pred": {
                            "name": "Llamada_Telefonica",
                            "varp": ["x"]
                        },
                        "predA": {
                            "name": "id_llamada",
                            "varp": ["x", "y"]
                        }
                    },
                    "predT": {
                        "name": "String",
                        "varp": ["y"]
                    }
                }
            }
        }, {
            "forall": {
                "var": ["x", "y"],
                "imply": {
                    "and": {
                        "pred": {
                            "name": "Llamada_Telefonica",
                            "varp": ["x"]
                        },
                        "predA": {
                            "name": "tipo",
                            "varp": ["x", "y"]
                        }
                    },
                    "predT": {
                        "name": "String",
                        "varp": ["y"]
                    }
                }
            }
        }, {
            "forall": {
                "var": ["x", "y"],
                "imply": {
                    "and": {
                        "pred": {
                            "name": "Llamada_Telefonica",
                            "varp": ["x"]
                        },
                        "predA": {
                            "name": "fecha",
                            "varp": ["x", "y"]
                        }
                    },
                    "predT": {
                        "name": "Date",
                        "varp": ["y"]
                    }
                }
            }
        }, {
            "forall": {
                "var": ["x", "y"],
                "imply": {
                    "and": {
                        "pred": {
                            "name": "Telefono",
                            "varp": ["x"]
                        },
                        "predA": {
                            "name": "id_telefono",
                            "varp": ["x", "y"]
                        }
                    },
                    "predT": {
                        "name": "String",
                        "varp": ["y"]
                    }
                }
            }
        }, {
            "forall": {
                "var": ["x", "y"],
                "imply": {
                    "and": {
                        "pred": {
                            "name": "Telefono",
                            "varp": ["x"]
                        },
                        "predA": {
                            "name": "numero",
                            "varp": ["x", "y"]
                        }
                    },
                    "predT": {
                        "name": "Int",
                        "varp": ["y"]
                    }
                }
            }
        }, {
            "forall": {
                "var": ["x", "y"],
                "imply": {
                    "and": {
                        "pred": {
                            "name": "Telefono",
                            "varp": ["x"]
                        },
                        "predA": {
                            "name": "codigo_pais",
                            "varp": ["x", "y"]
                        }
                    },
                    "predT": {
                        "name": "String",
                        "varp": ["y"]
                    }
                }
            }
        }, {
            "forall": {
                "var": ["x", "y"],
                "imply": {
                    "and": {
                        "pred": {
                            "name": "Telefono_Celular",
                            "varp": ["x"]
                        },
                        "predA": {
                            "name": "modelo",
                            "varp": ["x", "y"]
                        }
                    },
                    "predT": {
                        "name": "String",
                        "varp": ["y"]
                    }
                }
            }
        }, {
            "forall": {
                "var": ["x", "y"],
                "imply": {
                    "and": {
                        "pred": {
                            "name": "Telefono_Celular",
                            "varp": ["x"]
                        },
                        "predA": {
                            "name": "marca",
                            "varp": ["x", "y"]
                        }
                    },
                    "predT": {
                        "name": "String",
                        "varp": ["y"]
                    }
                }
            }
        }, {
            "forall": {
                "var": ["x", "y"],
                "imply": {
                    "and": {
                        "pred": {
                            "name": "Telefono_Celular",
                            "varp": ["x"]
                        },
                        "predA": {
                            "name": "peso",
                            "varp": ["x", "y"]
                        }
                    },
                    "predT": {
                        "name": "Double",
                        "varp": ["y"]
                    }
                }
            }
        }, {
            "forall": {
                "var": ["x", "y"],
                "imply": {
                    "and": {
                        "pred": {
                            "name": "Telefono_Fijo",
                            "varp": ["x"]
                        },
                        "predA": {
                            "name": "a\u00f1o_fabricacion",
                            "varp": ["x", "y"]
                        }
                    },
                    "predT": {
                        "name": "Integer",
                        "varp": ["y"]
                    }
                }
            }
        }, {
            "forall": {
                "var": ["x", "y"],
                "imply": {
                    "and": {
                        "pred": {
                            "name": "Telefono_Fijo",
                            "varp": ["x"]
                        },
                        "predA": {
                            "name": "tipo",
                            "varp": ["x", "y"]
                        }
                    },
                    "predT": {
                        "name": "String",
                        "varp": ["y"]
                    }
                }
            }
        }, {
            "forall": {
                "var": ["x", "y"],
                "imply": {
                    "and": {
                        "pred": {
                            "name": "Carrera",
                            "varp": ["x"]
                        },
                        "predA": {
                            "name": "id_carrera",
                            "varp": ["x", "y"]
                        }
                    },
                    "predT": {
                        "name": "String",
                        "varp": ["y"]
                    }
                }
            }
        }, {
            "forall": {
                "var": ["x", "y"],
                "imply": {
                    "and": {
                        "pred": {
                            "name": "Carrera",
                            "varp": ["x"]
                        },
                        "predA": {
                            "name": "nombre",
                            "varp": ["x", "y"]
                        }
                    },
                    "predT": {
                        "name": "String",
                        "varp": ["y"]
                    }
                }
            }
        }, {
            "forall": {
                "var": ["x", "y"],
                "imply": {
                    "and": {
                        "pred": {
                            "name": "Carrera",
                            "varp": ["x"]
                        },
                        "predA": {
                            "name": "a\u00f1o_creacion",
                            "varp": ["x", "y"]
                        }
                    },
                    "predT": {
                        "name": "Integer",
                        "varp": ["y"]
                    }
                }
            }
        }],
    "Links": [[{
                "forall": {
                    "var": ["x", "y"],
                    "imply": {
                        "pred": {
                            "name": "llama",
                            "varp": ["x", "y"]
                        },
                        "and": {
                            "pred": {
                                "name": "Telefono",
                                "varp": ["x"]
                            },
                            "predB": {
                                "name": "Llamada_Telefonica",
                                "varp": ["y"]
                            }
                        }
                    }
                }
            }, {
                "forall": {
                    "var": ["x"],
                    "imply": {
                        "pred": {
                            "name": "Telefono",
                            "varp": ["x"]
                        },
                        "multiplicity": {
                            "min": "1",
                            "#": {
                                "var": ["y"],
                                "pred": {
                                    "name": "llama",
                                    "varp": ["x", "y"]
                                }
                            },
                            "max": "*"
                        }
                    }
                }
            }, {
                "forall": {
                    "var": ["x"],
                    "imply": {
                        "pred": {
                            "name": "Llamada_Telefonica",
                            "varp": ["y"]
                        },
                        "multiplicity": {
                            "min": "1",
                            "#": {
                                "var": ["x"],
                                "pred": {
                                    "name": "llama",
                                    "varp": ["x", "y"]
                                }
                            },
                            "max": "1"
                        }
                    }
                }
            }], [{
                "forall": {
                    "var": ["x", "y"],
                    "imply": {
                        "pred": {
                            "name": "estudia",
                            "varp": ["x", "y"]
                        },
                        "and": {
                            "pred": {
                                "name": "Alumno",
                                "varp": ["x"]
                            },
                            "predB": {
                                "name": "Carrera",
                                "varp": ["y"]
                            }
                        }
                    }
                }
            }, {
                "forall": {
                    "var": ["x"],
                    "imply": {
                        "pred": {
                            "name": "Alumno",
                            "varp": ["x"]
                        },
                        "multiplicity": {
                            "min": "1",
                            "#": {
                                "var": ["y"],
                                "pred": {
                                    "name": "estudia",
                                    "varp": ["x", "y"]
                                }
                            },
                            "max": "*"
                        }
                    }
                }
            }, {
                "forall": {
                    "var": ["x"],
                    "imply": {
                        "pred": {
                            "name": "Carrera",
                            "varp": ["y"]
                        },
                        "multiplicity": {
                            "min": "1",
                            "#": {
                                "var": ["x"],
                                "pred": {
                                    "name": "estudia",
                                    "varp": ["x", "y"]
                                }
                            },
                            "max": "*"
                        }
                    }
                }
            }], [{
                "forall": {
                    "var": ["x", "y"],
                    "imply": {
                        "pred": {
                            "name": "trabaja",
                            "varp": ["x", "y"]
                        },
                        "and": {
                            "pred": {
                                "name": "Profesor",
                                "varp": ["x"]
                            },
                            "predB": {
                                "name": "Universidad",
                                "varp": ["y"]
                            }
                        }
                    }
                }
            }, {
                "forall": {
                    "var": ["x"],
                    "imply": {
                        "pred": {
                            "name": "Profesor",
                            "varp": ["x"]
                        },
                        "multiplicity": {
                            "min": "1",
                            "#": {
                                "var": ["y"],
                                "pred": {
                                    "name": "trabaja",
                                    "varp": ["x", "y"]
                                }
                            },
                            "max": "4"
                        }
                    }
                }
            }, {
                "forall": {
                    "var": ["x"],
                    "imply": {
                        "pred": {
                            "name": "Universidad",
                            "varp": ["y"]
                        },
                        "multiplicity": {
                            "min": "1",
                            "#": {
                                "var": ["x"],
                                "pred": {
                                    "name": "trabaja",
                                    "varp": ["x", "y"]
                                }
                            },
                            "max": "*"
                        }
                    }
                }
            }]],
    "IsA": [{
            "forall": {
                "var": "x",
                "imply": {
                    "pred": {
                        "name": "Profesor",
                        "varp": ["x"]
                    },
                    "predB": {
                        "name": "Persona",
                        "varp": ["x"]
                    }
                }
            }
        }, {
            "forall": {
                "var": "x",
                "imply": {
                    "pred": {
                        "name": "Alumno",
                        "varp": ["x"]
                    },
                    "predB": {
                        "name": "Persona",
                        "varp": ["x"]
                    }
                }
            }
        }, {
            "forall": {
                "var": "x",
                "imply": {
                    "pred": {
                        "name": "Cliente",
                        "varp": ["x"]
                    },
                    "predB": {
                        "name": "Persona",
                        "varp": ["x"]
                    }
                }
            }
        }, {
            "covering": [{
                    "forall": {
                        "var": ["x"],
                        "imply": {
                            "pred": {
                                "name": "Persona"
                            },
                            "or": [{
                                    "pred": {
                                        "name": "Profesor",
                                        "var": ["x"]
                                    }
                                }, {
                                    "pred": {
                                        "name": "Alumno",
                                        "var": ["x"]
                                    }
                                }, {
                                    "pred": {
                                        "name": "Cliente",
                                        "var": ["x"]
                                    }
                                }]
                        }
                    }
                }]
        }, {
            "forall": {
                "var": "x",
                "imply": {
                    "pred": {
                        "name": "Telefono_Celular",
                        "varp": ["x"]
                    },
                    "predB": {
                        "name": "Telefono",
                        "varp": ["x"]
                    }
                }
            }
        }, {
            "forall": {
                "var": "x",
                "imply": {
                    "pred": {
                        "name": "Telefono_Fijo",
                        "varp": ["x"]
                    },
                    "predB": {
                        "name": "Telefono",
                        "varp": ["x"]
                    }
                }
            }
        }, {
            "disjoint": [{
                    "forall": {
                        "var": ["x"],
                        "imply": {
                            "pred": {
                                "name": "Telefono_Celular",
                                "varp": ["x"]
                            },
                            "neg": {
                                "pred": {
                                    "name": "Telefono_Fijo",
                                    "varp": "x"
                                }
                            }
                        }
                    }
                }],
            "covering": [{
                    "forall": {
                        "var": ["x"],
                        "imply": {
                            "pred": {
                                "name": "Telefono"
                            },
                            "or": [{
                                    "pred": {
                                        "name": "Telefono_Celular",
                                        "var": ["x"]
                                    }
                                }, {
                                    "pred": {
                                        "name": "Telefono_Fijo",
                                        "var": ["x"]
                                    }
                                }]
                        }
                    }
                }]
        }]
}
EOT;
        
        
       /*  $strategy = new UMLFol();
         $strategy->create_fol($json);
//		print_r($strategy->fol);
        $this->assertJsonStringEqualsJsonString($expected, $strategy->get_json(), true); */
        
        $strategy = new UMLFol();
        $strategy->create_fol($json);
        
        
        $postagger = new PosTagger($json);
        $postagger->generate_SpGrammar();
        $empty_grammar= $postagger->get_SpGrammar();
        $verbs= $postagger->get_grammar_for_links();
        
        
        $conjugator = new VerbConjugator($verbs);
        $conjugator->conjugate_verbs();
        $verbs_conj = $conjugator->get_conjugate_verbs();
        
        //print_r("asdfasd \n");
        //print_r(array_keys($verbs_conj[0])[0]);
        //print_r(array_values($verbs_conj[0])[0]); 
       
             
        $crowdgrammar = new GrammarConnector();
        $crowdgrammar->run($empty_grammar);
        
        $grammar = $crowdgrammar->get_json_grammar();
        
        $verbalisation = new Verbalisation();
        
        //$verbalisation->setVerbs($verbs_conj);
        
        $verbalisation->generate_verbalisation($strategy->get_json(),$grammar,$verbs_conj);
        
        print_r($verbalisation->verbalisation);

        
    }

}
