<?php
/*

   Copyright 2018

   Author: GILIA

   jsondocumenttest.php

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

require_once("common.php");

//use function \load;
load("spgrammarjsondocument.php","wicom/translator/documents/");


use Wicom\Translator\Documents\SpGrammarJSONDocument;

class SpGrammarJSONDocumentTest extends PHPUnit\Framework\TestCase{

    public function testSpGrammarJSONConstructor(){
        $expected = '{"words":[]}';

        $d = new SpGrammarJSONDocument();
        $actual = $d->to_json();

        $this->assertJsonStringEqualsJsonString($expected, $actual, true);
        
        
        
    }

}

?>
