<?php
/*

   Copyright 2018

   Author: Giménez, Christian. Braun, Germán

   owllinkdocumenttest.php

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

require_once("common.php");

//use function \load;
load("owldocument.php","wicom/translator/documents/");


use Wicom\Translator\Documents\OWLDocument;

class OWL2DocumentTest extends PHPUnit\Framework\TestCase{

    public function testConstructor(){
        $expected = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>
          <Ontology
            xmlns=\"http://www.w3.org/2002/07/owl#\"
            xml:base=\"\"
            xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"
            xmlns:xml=\"http://www.w3.org/XML/1998/namespace\"
            xmlns:xsd=\"http://www.w3.org/2001/XMLSchema#\"
            xmlns:rdfs=\"http://www.w3.org/2000/01/rdf-schema#\"
            ontologyIRI=\"http://localhost/kb1\">
          </Ontology>";

        $d = new OWLDocument();
        $d->start_document("http://localhost/kb1");
        $d->end_document();
        $actual = $d->to_string();

        $expected = process_xmlspaces($expected);
        $actual = process_xmlspaces($actual);
        $this->assertEqualXMLStructure($expected, $actual, true);
    }

    public function testInsertOWL2ClassDeclaration(){
      $expected = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>
        <Ontology
          xmlns=\"http://www.w3.org/2002/07/owl#\"
          xml:base=\"\"
          xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"
          xmlns:xml=\"http://www.w3.org/XML/1998/namespace\"
          xmlns:xsd=\"http://www.w3.org/2001/XMLSchema#\"
          xmlns:rdfs=\"http://www.w3.org/2000/01/rdf-schema#\"
          ontologyIRI=\"http://localhost/kb1\">
          <Declaration>
            <Class IRI=\"Class1\"/>
          </Declaration>
        </Ontology>";

      $d = new OWLDocument();
      $d->start_document("http://localhost/kb1");
      $d->insert_class_declaration("Class1");
      $d->end_document();
      $actual = $d->to_string();

      $expected = process_xmlspaces($expected);
      $actual = process_xmlspaces($actual);
      $this->assertEqualXMLStructure($expected, $actual, true);
    }

    public function testInsertOWL2ObjectPropertyDeclaration(){
      $expected = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>
        <Ontology
          xmlns=\"http://www.w3.org/2002/07/owl#\"
          xml:base=\"\"
          xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"
          xmlns:xml=\"http://www.w3.org/XML/1998/namespace\"
          xmlns:xsd=\"http://www.w3.org/2001/XMLSchema#\"
          xmlns:rdfs=\"http://www.w3.org/2000/01/rdf-schema#\"
          ontologyIRI=\"http://localhost/kb1\">
          <Declaration>
            <ObjectProperty IRI=\"R1\"/>
          </Declaration>
        </Ontology>";

      $d = new OWLDocument();
      $d->start_document("http://localhost/kb1");
      $d->insert_objectProperty_declaration("R1");
      $d->end_document();
      $actual = $d->to_string();

      $expected = process_xmlspaces($expected);
      $actual = process_xmlspaces($actual);
      $this->assertEqualXMLStructure($expected, $actual, true);
    }


    public function testOWL2Subclass(){
      $expected = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>
        <Ontology
          xmlns=\"http://www.w3.org/2002/07/owl#\"
          xml:base=\"\"
          xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"
          xmlns:xml=\"http://www.w3.org/XML/1998/namespace\"
          xmlns:xsd=\"http://www.w3.org/2001/XMLSchema#\"
          xmlns:rdfs=\"http://www.w3.org/2000/01/rdf-schema#\"
          ontologyIRI=\"http://localhost/kb1\">
          <Declaration>
            <Class IRI=\"Class1\"/>
          </Declaration>
          <Declaration>
            <Class IRI=\"Class2\"/>
          </Declaration>
          <SubClassOf>
            <Class IRI=\"Class2\"/>
            <Class IRI=\"Class1\"/>
          </SubClassOf>
        </Ontology>";

      $d = new OWLDocument();
      $d->start_document("http://localhost/kb1");
      $d->insert_class_declaration("Class1");
      $d->insert_class_declaration("Class2");
      $d->insert_subclassof("Class2", "Class1");
      $d->end_document();
      $actual = $d->to_string();

      $expected = process_xmlspaces($expected);
      $actual = process_xmlspaces($actual);
      $this->assertEqualXMLStructure($expected, $actual, true);
    }

    public function testOWL2DisjointClasses(){
      $expected = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>
        <Ontology
          xmlns=\"http://www.w3.org/2002/07/owl#\"
          xml:base=\"\"
          xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"
          xmlns:xml=\"http://www.w3.org/XML/1998/namespace\"
          xmlns:xsd=\"http://www.w3.org/2001/XMLSchema#\"
          xmlns:rdfs=\"http://www.w3.org/2000/01/rdf-schema#\"
          ontologyIRI=\"http://localhost/kb1\">
          <Declaration>
            <Class IRI=\"Class1\"/>
          </Declaration>
          <Declaration>
            <Class IRI=\"Class2\"/>
          </Declaration>
          <DisjointClasses>
            <Class IRI=\"Class2\"/>
            <Class IRI=\"Class1\"/>
          </DisjointClasses>
        </Ontology>";

      $d = new OWLDocument();
      $d->start_document("http://localhost/kb1");
      $d->insert_class_declaration("Class1");
      $d->insert_class_declaration("Class2");
      $d->begin_disjointclasses();
        $d->insert_class("Class1");
        $d->insert_class("Class2");
      $d->end_disjointclasses();
      $d->end_document();
      $actual = $d->to_string();

      $expected = process_xmlspaces($expected);
      $actual = process_xmlspaces($actual);
      $this->assertEqualXMLStructure($expected, $actual, true);
    }

    public function testOWL2SubobjProp(){
      $expected = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>
        <Ontology
          xmlns=\"http://www.w3.org/2002/07/owl#\"
          xml:base=\"\"
          xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"
          xmlns:xml=\"http://www.w3.org/XML/1998/namespace\"
          xmlns:xsd=\"http://www.w3.org/2001/XMLSchema#\"
          xmlns:rdfs=\"http://www.w3.org/2000/01/rdf-schema#\"
          ontologyIRI=\"http://localhost/kb1\">
          <Declaration>
            <ObjectProperty IRI=\"R1\"/>
          </Declaration>
          <Declaration>
            <ObjectProperty IRI=\"R2\"/>
          </Declaration>
          <SubObjectPropertyOf>
            <ObjectProperty IRI=\"R2\"/>
            <ObjectProperty IRI=\"R1\"/>
          </SubObjectPropertyOf>
        </Ontology>";

      $d = new OWLDocument();
      $d->start_document("http://localhost/kb1");
      $d->insert_objectProperty_declaration("R1");
      $d->insert_objectProperty_declaration("R2");
      $d->insert_subobjectpropertyof("R2", "R1");
      $d->end_document();
      $actual = $d->to_string();

      $expected = process_xmlspaces($expected);
      $actual = process_xmlspaces($actual);
      $this->assertEqualXMLStructure($expected, $actual, true);
    }

    /* Tests for checking OWLlink ClassHierarchy Responses  */

    /*  <xsd:element name="ClassHierarchy">
          <xsd:complexType>
            <xsd:complexContent>
              <xsd:extension base="ol:KBResponse">
                <xsd:sequence><!-- the synset of unsatisfiable classes -->
                  <xsd:element name="ClassSynset" type="ol:ClassSynset"/>
                  <xsd:element name="ClassSubClassesPair" minOccurs="0" maxOccurs="unbounded">
                    <xsd:complexType>
                      <xsd:sequence>
                        <xsd:element name="ClassSynset" type="ol:ClassSynset"/>
                        <xsd:element ref="ol:SubClassSynsets"/>
                      </xsd:sequence>
                    </xsd:complexType>
                  </xsd:element>
                </xsd:sequence>
              </xsd:extension>
            </xsd:complexContent>
          </xsd:complexType>
        </xsd:element>  */

/*    public function testClassHierarchyResponse(){
        $expected = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>
<ResponseMessage xmlns=\"http://www.owllink.org/owllink#\"
                 xmlns:owl=\"http://www.w3.org/2002/07/owl#\">
    <OK/>
    <ClassHierarchy>
      <ClassSynset>
        <owl:Class abbreviatedIRI=\"owl:Nothing\"/>
      </ClassSynset>
      <ClassSubClassesPair>
        <ClassSynset>
          <owl:Class abbreviatedIRI=\"owl:Thing\"/>
        </ClassSynset>
        <SubClassSynsets>
          <ClassSynset>
            <owl:Class IRI=\"Person\"/>
          </ClassSynset>
        </SubClassSynsets>
      </ClassSubClassesPair>
      <ClassSubClassesPair>
        <ClassSynset>
          <owl:Class IRI=\"Person\"/>
        </ClassSynset>
        <SubClassSynsets>
          <ClassSynset>
            <owl:Class IRI=\"Director\"/>
          </ClassSynset>
          <ClassSynset>
            <owl:Class IRI=\"Employer\"/>
          </ClassSynset>
          <ClassSynset>
            <owl:Class IRI=\"Employee\"/>
          </ClassSynset>
        </SubClassSynsets>
      </ClassSubClassesPair>
    </ClassHierarchy>
  </ResponseMessage>";

        $d = new OWLlinkDocumentResponse();
        $d->start_document_response();
        $d->insert_kb_response("http://localhost/kb1");
        $d->insert_kb_response("OK");
        $d->start_class_hierarchy();
        $d->insert_class_synset_bottom();
        $d->insert_classSubClassesPair("owl:Thing","Person");
        $d->insert_classSubClassesPair("Person",["Director", "Employer", "Employee"]);
        $d->end_class_hierarchy();
        $d->end_document_response();

        $actual = $d->to_string();

        $expected = process_xmlspaces($expected);
        $actual = process_xmlspaces($actual);
        $this->assertEqualXMLStructure($expected, $actual, true);
    }*/
}

?>
