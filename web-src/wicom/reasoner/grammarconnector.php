<?php
/*

   Copyright 2018 GILIA

   Author: GILIA

   grammarconnector.php

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Wicom\Reasoner;

load("connector.php");
load("config.php", "../../config/");

use Wicom\Reasoner\Connector;

class GrammarConnector extends Connector{


    //TODO: Change PROGRAM_CMD and FILES_PATH into configuration variables.

    /**
       The CrowdGrammar command to execute with all its parameters.
     */
    const PROGRAM_CMD = "Gramatica.jar";
    const PROGRAM_PARAMS = "java -jar ";

    /**
       Execute CrowdGrammar with the given $document as input.
     */
    function run($input_string){
        $temporal_path = $GLOBALS['config']['temporal_path'];
        $crowdgrammar_path = $GLOBALS['config']['temporal_path'];

        //$temporal_path = realpath($temporal_path) . "/";
                $file_path = $temporal_path . "grammar.json";
                $out_path = $temporal_path . "gramatica-out.json";
        $crowdgrammar_path .= GrammarConnector::PROGRAM_CMD;
                 $commandline =GrammarConnector::PROGRAM_PARAMS . $crowdgrammar_path . " " .  $file_path . " " . $out_path;
                 
        //$commandline = GrammarConnector::PROGRAM_PARAMS . $crowdgrammar_path . " " . $input_string;
                // print_r("La ruta de entrada es ".$file_path);
/*                // print_r("La ruta de salida es ".$out_path);
        
           $this->check_files($temporal_path, $crowdgrammar_path, $file_path);
*/
                 
        $crowd_file = fopen($file_path, "w");

        if (! $crowd_file){
            throw new \Exception("Temporal file could not be opened for writing...
            Is the path '$file_path' correct?");
        }

        fwrite($crowd_file, $input_string);
        fclose($crowd_file); 
                 
        $this->check_files($temporal_path, $crowdgrammar_path, $file_path);

        exec($commandline,$answer);

        array_push($this->col_answers, join($answer));
    }

    /**
       Check for program and input file existance and proper permissions.

       @return true always
       @exception Exception with proper message if any problem is founded.
    */
    function check_files($temporal_path, $grammarcrowd_path, $file_path){
        if (! is_dir($temporal_path)){
            throw new \Exception("Temporal path desn't exists!
Are you sure about this path?
temporal_path = \"$temporal_path\"");
        }

        if (!file_exists($file_path)){
            throw new \Exception("Temporal file doesn't exists, please create one at '$file_path'.");
        }

        if (!is_readable($file_path)){
            throw new \Exception("Temporal file cannot be readed.
Please set the write and read permissions for '$file_path'");
        }

        if (file_exists($file_path) and !is_writable($file_path)){
            throw new \Exception("Temporal file is not writable, please change the permissions.
Check the permissions on '${file_path}'.");
        }

        return true;
    }
    
    function get_json_grammar(){
        $temporal_path = $GLOBALS['config']['temporal_path'];
        $out_path = $temporal_path . "gramatica-out.json";
        
        //$crowd_file = fopen($out_path, "r");
        //print_r("La ruta es ".$out_path);
        $str = file_get_contents($out_path);
        //fclose($crowd_file); 
        
        $grammar= json_decode( preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $str), true );
        //$var= json_decode($str,true);
        
        return $grammar;
        
    }
}

?>
