<?php
/*

   Copyright 2017 Giménez, Christian

   Author: Giménez, Christian

   owldocument.php

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Wicom\Translator\Documents;

use function \load;
load('document.php');

use function \preg_match;
use \XMLWriter;

/**

   # Example

   @code{.php}
   $d = new OWLlinkDocument();
   $d->insert_startdocument();
   $d->insert_request();

   // ...

   $d->end_document();

   $d->to_string();
   @endcode

 */
class OWLDocument extends Document{
    protected $content = null;

    protected $owllink_text = "";

    function __construct(){
        $this->content = new XMLWriter();
        $this->content->openMemory();
    }

    /**
       @name Starting and Ending the document
    */
    ///@{

    public function insert_startdocument(){
        $this->content->startDocument("1.0", "UTF-8");
    }

    public function insert_ontology($iri){
        $this->content->startElement("Ontology");
        $this->content->writeAttribute("xmlns", "http://www.w3.org/2002/07/owl#");
        $this->content->writeAttribute("xml:base","");
        $this->content->writeAttribute("xmlns:rdf", "http://www.w3.org/1999/02/22-rdf-syntax-ns#" );
        $this->content->writeAttribute("xmlns:xml", "http://www.w3.org/XML/1998/namespace" );
		    $this->content->writeAttribute("xmlns:xsd", "http://www.w3.org/2001/XMLSchema#");
        $this->content->writeAttribute("xmlns:rdfs", "http://www.w3.org/2000/01/rdf-schema#");
		    $this->content->writeAttribute("ontologyIRI", $iri);
    }

    /**
       Abbreviation of:

       @code{.php}
       $d->insert_startdocument();
       $d->insert_request();
       @endcode
     */
    public function start_document($iri){
        $this->insert_startdocument();
        $this->insert_ontology($iri);
    }

    public function end_document(){
        $this->content->endElement();
    }

    ///@}
    // Starting and ending the document

    public function insert_prefix($uri){
    }

    /**
       Insert a DL subclass-of operator.

       Abbreviated IRIs are recognized automatically.

       @param child_class A String with the child's name class.
       @param father_class Same as $child_class parameter but for
       the $father_class.
       @param child_abbrev If true, force the abbreviated IRI for the
       child class; if false, force the (not abbreviated) IRI; if
       null check it automatically.
       @param father_abbrev same as $child_abbrev but for the
       $father_class.
     */

    public function insert_class_declaration($class, $class_abbrev=false){
        $this->content->startElement("Declaration");
        $this->insert_class($class, $class_abbrev);
        $this->content->endElement();
    }

    public function insert_objectProperty_declaration($objprop, $objprop_abbrev=false){
        $this->content->startElement("Declaration");
        $this->insert_objectproperty($objprop, $objprop_abbrev);
        $this->content->endElement();
    }

    public function insert_subclassof($child_class, $father_class, $child_abbrev=false, $father_abbrev=false){
        $this->content->startElement("SubClassOf");
        $this->insert_class($child_class, $child_abbrev);
        $this->insert_class($father_class, $father_abbrev);
        $this->content->endElement();
    }

    public function insert_subobjectpropertyof($child_objprop, $father_objprop, $child_abbrev=false, $father_abbrev=false){
        $this->content->startElement("SubObjectPropertyOf");
        $this->insert_objectproperty($child_objprop, $child_abbrev);
        $this->insert_objectproperty($father_objprop, $father_abbrev);
        $this->content->endElement();
    }

    /**
       Check if this IRI has a namespace, (i.e.: is an
       abbreviated IRI).

       Like in "owl:Thing" which its namespace is "owl" here.

       @param name a String with the IRI.
       @return True if the name has an XML Namespace. False otherwise.
    */
    protected function name_has_namespace($name){
        $ns_regexp = '/.*:.*/';        // Namespace Regexp.

        if (preg_match($ns_regexp, $name) > 0){
            return true;
        }else{
            return false;
        }
    }

    /**
       Add a class DL element.

       Abbreviated IRI's are recognized automatically by name_has_namespace() function.

       @param name String the name or IRI of the new concept.
       @param is_abbreviated Boolean (Optional) force that the given IRI is or is not an abreviated like <tt>owl:class</tt>.
     */
    public function insert_class($name, $is_abbreviated=null){
        if ($is_abbreviated == null){
            // Is abbreviated is not forced, so check namepsace
            // presence...
            $is_abbreviated = $this->name_has_namespace($name);
        }

        $this->content->startElement("Class");
        if ($is_abbreviated){
            $this->content->writeAttribute("abbreviatedIRI", $name);
        }else{
            $this->content->writeAttribute("IRI", '#'.$name);
        }
        $this->content->endElement();
    }

    public function insert_objectproperty($name){
        $this->content->startElement("ObjectProperty");
        $this->content->writeAttribute("IRI", '#'.$name);
        $this->content->endElement();
    }

    public function begin_inverseof(){
        $this->content->startElement("ObjectInverseOf");
    }
    public function end_inverseof(){
        $this->content->EndElement();
    }
    public function begin_subclassof(){
        $this->content->startElement("SubClassOf");
    }
    public function end_subclassof(){
        $this->content->EndElement();
    }
    public function begin_intersectionof(){
        $this->content->startElement("ObjectIntersectionOf");
    }
    public function end_intersectionof(){
        $this->content->EndElement();
    }

    public function begin_unionof(){
        $this->content->startElement("ObjectUnionOf");
    }
    public function end_unionof(){
        $this->content->EndElement();
    }

    public function begin_complementof(){
        $this->content->startElement("ObjectComplementOf");
    }
    public function end_complementof(){
        $this->content->EndElement();
    }

    public function begin_somevaluesfrom(){
        $this->content->startElement("ObjectSomeValuesFrom");
    }
    public function end_somevaluesfrom(){
        $this->content->EndElement();
    }

    public function begin_allvaluesfrom(){
        $this->content->startElement("ObjectAllValuesFrom");
    }
    public function end_allvaluesfrom(){
        $this->content->EndElement();
    }

    public function begin_mincardinality($cardinality){
        $this->content->startElement("ObjectMinCardinality");
        $this->content->writeAttribute("cardinality", $cardinality);
    }
    public function end_mincardinality(){
        $this->content->EndElement();
    }
    public function begin_maxcardinality($cardinality){
        $this->content->startElement("ObjectMaxCardinality");
        $this->content->writeAttribute("cardinality", $cardinality);
    }
    public function end_maxcardinality(){
        $this->content->EndElement();
    }

	public function begin_objectpropertydomain(){
		$this->content->startElement("ObjectPropertyDomain");
	}

   	public function end_objectpropertydomain(){
        $this->content->EndElement();
	}

	public function begin_objectpropertyrange(){
		$this->content->startElement("ObjectPropertyRange");
	}

   	public function end_objectpropertyrange(){
        $this->content->EndElement();
	}

	public function begin_equivalentclasses(){
		$this->content->startElement("EquivalentClasses");
	}

  public function end_equivalentclasses(){
        $this->content->EndElement();
	}

  public function begin_disjointclasses(){
      $this->content->startElement("DisjointClasses");
  }

  public function end_disjointclasses(){
        $this->content->EndElement();
  }

    /**
       Insert an ASK query denominated IsEntailedDirect for all the classes in the array.

       @param $array An array of Strings with classnames.
     */
    public function insert_equivalent_class_query($array){
        $this->content->startelement("EquivalentClasses");
        foreach ($array as $classname){
            $this->content->startElement("Class");
            $this->content->writeAttribute("IRI", "#" . $classname);
            $this->content->endElement();
        }
        $this->content->endElement();
    }

    public function to_string(){
        $str = $this->content->outputMemory();
        return $str;
    }

    public function insert_owl2($text){
        $this->content->writeRaw($text);

    }
}

?>
