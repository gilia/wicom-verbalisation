<?php
/*

Copyright 2018

Author: GILIA

document.php

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

namespace Wicom\Translator\Documents;

use function \load;
load('jsondocument.php');

/**
Json structure for grammatical analysis

{"words":[
	{"name":"Persona", "grammar" : ["gender": "",
																 "number" : "",
																 "plural" : "",
																 "syllabes" : "",
																 "capitalise" : "",
															 	 "article" : [],
															 	 "preposition" : []},

  {"name": "", "grammar" : []}
	]
}

*/

class SpGrammarJSONDocument extends JSONDocument{

	protected $content = [];

	protected $grammar = [
		"gender" => "",
		"number" => "",
		"plural" => "",
		"capitalise" => "",
		"article" => [],
		"preposition" => []
	];

	function __construct(){
		$this->content = ["words" => []];
	}

	function add_word($word){
		array_push($this->content["words"],[
			"name" => $word,
			"grammar" => $this->grammar,
		]);
	}

	public function to_json(){
		return json_encode($this->content);
	}
        
        
}



?>
