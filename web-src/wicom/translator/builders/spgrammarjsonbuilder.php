<?php
/*

Copyright 2018

Author: Garrido, Matias. Braun, Germán

jsonbuilder.php

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

namespace Wicom\Translator\Builders;

use function \load;
load("jsonbuilder.php");
load("spgrammarjsondocument.php", "../documents/");

use Wicom\Translator\Documents\SpGrammarJSONDocument;

/**
 I set the common behaviour for every DocumentBuilder subclass.

 @abstract
 */


class SpGrammarJSONBuilder extends Documents{
    
    protected $product= NULL;

	public function __construct(){
		$this->product = new SpGrammarJSONDocument();
	}

	public function add_word($word){
		$this->product->add_word($word);
	}

	public function get_product(){
            return $this->product;
	}
        
}
?>
