<?php 
/* 

   Copyright 2016 GILIA, Departamento de Teoría de la Computación, Universidad Nacional del Comahue
   
   Author: GILIA

   crowd_uml.php
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Wicom\Translator\Fol;

use function \load;

load("umlPredicates.php");

use Wicom\Translator\Fol\ClassPredicate;
use Wicom\Translator\Fol\IsAPredicate;
use Wicom\Translator\Fol\Attribute;
use Wicom\Translator\Fol\Association;
use Wicom\Translator\Fol\IsAConstraints;

class UMLFol{

	/**
	 Translate a given JSON String representing a UML class diagram into a new JSON metamodel string.
	
	 @param json JSON_UML string
	 @return a JSON_FOL string.
	 */
    
	public $fol;
	
	function __construct(){
		$this->fol = ["Classes" => [],"Attribute" => [],"Links"=> [] ,"IsA" => []];
	}
	    
    function create_fol($json_str){
        $json = json_decode($json_str, true);                                                       
        
        
        $this->translateClassesToFol($json);
        $this->translateIsA($json);
        $this->translateLinks($json);
    }
    
    
    function translateClassesToFol($json){
        $js_classes = $json["classes"];
    	foreach ($js_classes as $class){
    		$class_predicate = new ClassPredicate($class["name"]);
    		array_push($this->fol["Classes"],$class_predicate->get_json_array());
                    
        $js_attr = $class["attrs"];            
        if (!empty($js_attr)){
    			foreach ($js_attr as $attr){
    				$attr_obj = new Attribute($class["name"],$attr["name"],$attr["datatype"]);
    				array_push($this->fol["Attribute"],$attr_obj->get_json_array());
    			}
    		}
    	}
    }
    
     function translateIsA($json) {
        $js_links = $json["links"];
        $gen_classes = array_filter($js_links, function($gen) {
            return $gen["type"] == "generalization";
        });
        foreach ($gen_classes as $gen_class) {
            $children = $gen_class["classes"];
            foreach ($children as $child) {
                $isA_predicate = new IsAPredicate($gen_class["parent"], $child);
                array_push($this->fol["IsA"], $isA_predicate->get_json_array());
            }
            $parent= $gen_class["parent"];
            
            $constraints = $gen_class["constraint"];
            
            if (!empty($constraints)) {
                $isA_Constraints=new IsAConstraints($constraints,$parent,$children); //implementar ésto (hijos,restricciones)  
                array_push($this->fol["IsA"], $isA_Constraints->get_json_array());  //agrego las restricciones
            }
        }
    }

    function get_json(){
    	return json_encode($this->fol,true);
    	
    }

    
    function translateLinks($json) {
        $js_links = $json["links"];
        foreach ($js_links as $link) {
            switch ($link["type"]) {
                case "association":
                    $classes=$link["classes"];
                    $multiplicities=$link["multiplicity"];
                    $name_association=$link["name"];
                    $association = new Association($classes,$multiplicities,$name_association,null);
                    array_push($this->fol["Links"],$association->get_json_array());
                    break;
                //case "generalization":
                  //  $this->translate_generalization($link, $builder);
                 //   break;
            }
        }
    }
    
  /*protected function translate_links($json, $builder){
        if (! array_key_exists("links", $json)){
            return false;
        }
        $js_links = $json["links"];
        foreach ($js_links as $link){
            switch ($link["type"]){
            case "association":
                $this->translate_association_without_class($link, $builder);
                break;
            case "generalization":
                $this->translate_generalization($link, $builder);
                break;
            }
        }
        
    } */
    
}