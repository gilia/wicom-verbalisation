<?php
/*

   Copyright 2016 GILIA

   Author: Giménez, Christian. Braun, Germán

   crowdanalizer.php

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Wicom\Translator\Strategies\QAPackages\AnswerAnalizers;


//load("documentbuilder.php", "../../../../builders/");
//load("owllinkdocument.php", "../../../../documents/");
load("owllinkbuilder.php", "../../../builders/");
load("documentbuilder.php", "../../../builders/");
load("owlbuilder.php", "../../../builders/");
load("answer.php");
load("ansanalizer.php");

//use Wicom\Translator\Documents\OWLlinkDocument;
use Wicom\Translator\Builders\OWLlinkBuilder;
use Wicom\Translator\Builders\OWLBuilder;
use Wicom\Translator\Builders\DocumentBuilder;
use Wicom\Translator\Strategies\QAPackages\AnswerAnalizers\Answer;
use Wicom\Translator\Strategies\QAPackages\AnswerAnalizers\AnsAnalizer;
use \XMLReader;
use \SimpleXMLElement;
use \SimpleXMLIterator;



/**
   crowd Answer Analizer. Analise the answers and generate a new OWLlink document for the new Ontology!

   <IsKBSatisfiable kb="http://localhost/kb1"/>    <OK/>
   <IsClassSatisfiable kb="http://localhost/kb1"> <BooleanResponse result="true"/>
   <GetSubClassHierarchy kb="http://localhost/kb1"/>   <ClassHierarchy>
                                                          <ClassSynset>
                                                            <owl:Class abbreviatedIRI="owl:Nothing"/>
                                                          </ClassSynset>
                                                          <ClassSubClassesPair>
                                                            <ClassSynset>
                                                              <owl:Class abbreviatedIRI="owl:Thing"/>
                                                            </ClassSynset>
                                                            <SubClassSynsets>
                                                              <ClassSynset>
                                                                <owl:Class IRI="A"/>
                                                              </ClassSynset>
                                                            </SubClassSynsets>
                                                          </ClassSubClassesPair>
                                                      </ClassHierarchy>
    <GetDisjointClasses kb="http://localhost/kb1">   <ClassSynsets>
                                                        <ClassSynset>
                                                          <owl:Class abbreviatedIRI="owl:Nothing"/>
                                                        </ClassSynset>
                                                     </ClassSynsets>
    <GetEquivalentClasses kb="http://localhost/kb1"> <SetOfClasses>
                                                        <owl:Class IRI="A"/>
                                                     </SetOfClasses>

 */
class CrowdAnalizer extends AnsAnalizer{

    /**
       XMLReader instance for parsing the query given to the
       reasoner.
     */
    protected $query_reader = null;
    /**
       XMLReader instance for parsing the reasoner answer.
     */
    protected $answer_reader = null;

    /**
       Map between Queries and propper correct answers
       This method also generates the new ontology owllink after reasoning.

       Used for filtering XML tags to the ones we care.
     */
    const ANSWERS_MAP = [
        "IsKBSatisfiable" => "BooleanResponse",
        "IsClassSatisfiable" => "BooleanResponse",
        "GetSubClassHierarchy" => "ClassHierarchy",
        "GetDisjointClasses" => "ClassSynsets",
        "GetEquivalentClasses" => "SetOfClasses",
        "IsObjectPropertySatisfiable" => "BooleanResponse",
        "GetSubObjectPropertyHierarchy" => "ObjectPropertyHierarchy",
        "GetDisjointObjectProperties" => "ObjectPropertySynsets",
        "GetEquivalentObjectProperties" => "SetOfObjectProperties",
    ];

    function generate_answer($query, $answer, $owl2){
        parent::generate_answer($query, $answer, $owl2);
    }

    /**
    This function is for going until the first OWLlink query.
    */

    function goto_first_query(){
      $this->owllink_queries->rewind();
      $first_query = key(CrowdAnalizer::ANSWERS_MAP);

      while (($this->owllink_queries->valid()) and ($first_query != NULL) and
             ($this->owllink_queries->current()->getName() != $first_query)){

               $this->owllink_queries->next();
      }
    }

    /**
    This function is for going until the first OWLlink response.
    */

    function goto_first_response(){
      $this->owllink_responses->rewind();
      $first_response = CrowdAnalizer::ANSWERS_MAP[key(CrowdAnalizer::ANSWERS_MAP)];

      while (($this->owllink_responses->valid()) and ($first_response != NULL) and
             ($this->owllink_responses->current()->getName() != $first_response)){

               $this->owllink_responses->next();
      }
    }

    function get_current_owlclass($query = true){
      if ($query){
          return $this->owllink_queries->current()->children("owl",TRUE);
      }
      else {
        return $this->owllink_responses->current()->children("owl",TRUE);
      }
    }

    /**
    Parsing OWLlink <ClassHierarchy> tag. This function returns an array of subhierarchies.
    <ClassSubClassesPair>
                  <ClassSynset>
                    <owl:Class abbreviatedIRI="owl:Thing"/>
                  </ClassSynset>
                  <SubClassSynsets>
                    <ClassSynset>
                      <owl:Class IRI="A"/>
                    </ClassSynset>
                  </SubClassSynsets>
                </ClassSubClassesPair>
    */

    function parse_owllinkhierarchy(){
      $hierarchies = [];

      foreach ($this->owllink_responses->current()->children() as $first_children) {
        //<ClassSynset><owl:Class abbreviatedIRI="owl:Nothing"/></ClassSynset>
        $tag_first_child = $first_children->getName();

        if (($tag_first_child != "ClassSynset") and ($tag_first_child = "ClassSubClassesPair")){
          $hierarchy = [];
//          ;
/*          <ClassSubClassesPair>
              <ClassSynset>
                <owl:Class abbreviatedIRI="owl:Thing"/>
              </ClassSynset>
              <SubClassSynsets>
                <ClassSynset>
                  <owl:Class IRI="A"/>
                </ClassSynset>
              </SubClassSynsets>
            </ClassSubClassesPair> */
          foreach ($first_children->children() as $second_children){ //<ClassSynset>
            $tag_second_child = $second_children->getName();

            switch ($tag_second_child){

              case "ClassSynset":
                $class_parent = $second_children->children("owl",TRUE);

                if ($class_parent->count() > 0){
                  $class_parent_name = $class_parent[0]->attributes()[0];
                }
                array_push($hierarchy,$class_parent_name->__toString());  // insert parent
                break;

              case "SubClassSynsets" :
                foreach ($second_children->children() as $third_children){
                  $tag_third_child = $third_children->getName();

                  if ($tag_third_child = "ClassSynset"){
                    $class_child = $third_children->children("owl",TRUE);

                    if ($class_child->count() > 0){
                      $class_child_name = $class_child[0]->attributes()[0];
                    }
                  }
                  array_push($hierarchy,$class_child_name->__toString()); // insert child
                }
              }
          }
          array_push($hierarchies,$hierarchy);
        }
      }
      return $hierarchies;
    }

    /**
    Parsing OWLlink <ClassSynsets> tag. This function returns an array of disjoint classes.
    <ClassSynsets>
      <ClassSynset>
        <owl:Class abbreviatedIRI="owl:Nothing"/>
      </ClassSynset>
      <ClassSynset>
        <owl:Class IRI="B"/>
      </ClassSynset>
    </ClassSynsets>
    */

    function parse_owllinkdisjoint(){
      $disjoint = [];

      foreach ($this->owllink_responses->current()->children() as $children){ //<ClassSynset>
        $tag_child = $children->getName();

        if ($tag_child = "ClassSynset"){
            $class = $children->children("owl",TRUE);

            if ($class->count() > 0){
              $class_name = $class[0]->attributes()[0];
            }
            array_push($disjoint,$class_name->__toString());
        }
      }
      return $disjoint;
    }

    /**
    Parsing OWLlink <SetOfClasses> tag. This function returns an array of equivalent classes.
    <SetOfClasses>
      <owl:Class IRI="D"/>
      <owl:Class IRI="C"/>
    </SetOfClasses>
    */
    function parse_owllinkequivalent(){
      $equivalent = [];

      $owl_children = $this->owllink_responses->current()->children("owl",TRUE);

      foreach ($owl_children as $child){
        $class_name = $child[0]->attributes()[0];
        array_push($equivalent,$class_name->__toString());
      }

      return $equivalent;
    }


    /**
    This function starts parsing OWLlink responses file using an Concrete Iterator and outs a new
    array of responses to be inserted in the new ontology.
    It delegates to SimpleXMLIterator and OWLlinkBuilder
    */

    function get_responses(){

      $bool_responses = [
          "IsKBSatisfiable" => "false",
          "IsClassSatisfiable" => [],
          "GetSubClassHierarchy" => [],
          "GetDisjointClasses" => [],
          "GetEquivalentClasses" => [],
          "DL" => []
      ];

      $this->goto_first_query();
      $this->goto_first_response();

      while (($this->owllink_responses->valid()) and ($this->owllink_queries->valid())){

            $name_query = $this->owllink_queries->current()->getName();

            switch ($name_query){
              case "IsKBSatisfiable":
                $name_response = $this->owllink_responses->current()->getName();
                if ($name_response = "BooleanResponse"){
                  $attr_response = $this->owllink_responses->current()->attributes()["result"];
                  $bool_responses[$name_query] = $attr_response->__toString();
                }
                break;

              case "IsClassSatisfiable":
                $name_response = $this->owllink_responses->current()->getName();
                if ($name_response = "BooleanResponse"){
                  $attr_response = $this->owllink_responses->current()->attributes()["result"];
                  $class_query = $this->get_current_owlclass();
                  if ($class_query->count() > 0){
                    $class_name = $class_query[0]->attributes()["IRI"];
                    array_push($bool_responses[$name_query], [$attr_response->__toString(),$class_name->__toString()]);
                  }
                }
                break;

              case "GetSubClassHierarchy":   // {father,[childs]}
                $name_response = $this->owllink_responses->current()->getName();
                if ($name_response = "ClassHierarchy"){
                  $hierarchy = $this->parse_owllinkhierarchy();
                  array_push($bool_responses[$name_query],$hierarchy);
                }
                foreach ($hierarchy as $subhier) {
                  $parent = $subhier[0];
                  $counter = 1;
                  while ($counter < count($subhier)){
                    array_push($bool_responses["DL"],["subclass" => [
                                                      ["class" => $subhier[$counter]],
                                                      ["class" => $parent]]]);
                    $counter = $counter + 1;
                  }
                }
                break;

              case "GetDisjointClasses":   // {class,[disjointclasses]}
                $name_response = $this->owllink_responses->current()->getName();
                if ($name_response = "ClassSynsets"){
                  $class_query = $this->get_current_owlclass();
                  if ($class_query->count() > 0){
                    $class_name = $class_query[0]->attributes()["IRI"];
                    $disjoint = $this->parse_owllinkdisjoint();
                    $class_d = $class_name->__toString();
                    array_push($bool_responses[$name_query],[$class_d,$disjoint]);
                  }
                }
                foreach ($disjoint as $disjoint_class) {
                  array_push($bool_responses["DL"],["disjointclasses" => [
                                                      ["class" => $class_d],
                                                      ["class" => $disjoint_class]]]);
                }
                break;

              case "GetEquivalentClasses":
                $name_response = $this->owllink_responses->current()->getName();
                if ($name_response = "SetOfClasses"){
                  $class_query = $this->get_current_owlclass();
                  if ($class_query->count() > 0){
                    $class_name = $class_query[0]->attributes()["IRI"];
                    $equivalent = $this->parse_owllinkequivalent();
                    $class_e = $class_name->__toString();
                    array_push($bool_responses[$name_query],[$class_e,$equivalent]);
                  }
                }
                foreach ($equivalent as $equiv_class) {
                  array_push($bool_responses["DL"],["equivalentclasses" => [
                                                      ["class" => $class_e],
                                                      ["class" => $equiv_class]]]);
                }
                break;
            }
            $this->owllink_responses->next();
            $this->owllink_queries->next();
      }

      return $bool_responses;
    }


    /**
       Remove comments and other tags, and create an array with
       the most important tags and its answer.

       @return An Asociated Array with most important tags and
       its answer. Example:

       @code{.php}
       [
         "IsKBSatisfiable" => "true",
         "IsClassSatisfiable" => "true"
       ]
       @endcode
     */
    function filter_xml_responses(){

        $cont = true;

        $col = [
            "IsKBSatisfiable" => "false",
            "IsClassSatisfiable" => []
        ];

        // Enter the first tag
        $this->next_tag();
        $this->next_tag(false);
        $this->query_reader->read();
        $this->answer_reader->read();


        while ($this->next_important_tag()){
            if (! $this->next_important_tag(false)){
                return $col;
            }

            $a_name = $this->answer_reader->name;
            $q_name = $this->query_reader->name;

            if ($a_name == "BooleanResponse") {
                $isit = $this->answer_reader->getAttribute("result");

                if ($q_name == "IsClassSatisfiable"){
                    $classname = $this->find_classname();
                    array_push($col[$q_name], [$isit, $classname]);
                }else{  // $q_name == "IsKBSatisfiable"
                    $col[$q_name] = $isit;
                }
            }
        }

        return $col;
    }

    protected function find_classname(){
        $this->query_reader->read();
        while ($this->query_reader->name != "owl:Class"){
            if (! $this->next_tag()){
                return false;
            }
        }

        $classname = $this->query_reader->getAttribute("IRI");

        return $classname;
    }

    /**
       Simply, go to the next initial tag.

       @param in_query A boolean, if true advance the query_reader,
       if false, advance the answer_reader.

       @return false if the EOF has been reached.
     */
    protected function next_tag($in_query = true){
        $cont = true;
        if ($in_query){
            $xml = $this->query_reader;
        }else{
            $xml = $this->answer_reader;
        }

        while ($cont){
            try{
                if (!$xml->next()){
                    return false;
                }
            }catch(Exception $e){
                return false;
            }
            $cont = $xml->nodeType != XMLReader::ELEMENT;
        }
        return true;
    }

    /**
       @param in_query A Boolean indicating if it must advance the
       $this->query_reader XMLReader instance (true) or the
       $this->answer_reader one (false).

       @return false if the EOF has been reached. True otherwise.
     */
    protected function next_important_tag($in_query=true){
        $cont = true;

        if ($in_query){
            $xml = $this->query_reader;
        }else{
            $xml = $this->answer_reader;
        }

        while ($cont){
            if (! $xml->next()){
                // Cannot read more, EOF?
                return false;
            }

            // If it is not an ELEMENT, repeat.
            $cont = $xml->nodeType != XMLReader::ELEMENT;

            // If it has not the tagname we're looking for, repeat.
            if (!$cont){
                // It is in fact an element.
                if($in_query){
                    $cont = ! array_key_exists($xml->name,
                                               CrowdAnalizer::ANSWERS_MAP);
                }else{
                    $cont = ! array_search($xml->name,
                                           CrowdAnalizer::ANSWERS_MAP);
                }
            }
        }

        return true;
    }



    function analize(){

        $responses = $this->get_responses();

        $val = $responses["IsKBSatisfiable"];
        $this->answer->set_kb_satis($val == "true");

        $col_class = $responses["IsClassSatisfiable"];
        foreach ($col_class as $val){
            if ($val[0] == "true"){
                $this->answer->add_satis_class($val[1]);
            }else{
                $this->answer->add_unsatis_class($val[1]);
            }
        }

        $this->answer->start_owl2_answer();
        $this->answer->translate_responses($responses["DL"]);
        $this->answer->copyowl2_to_response();
        $this->answer->end_owl2_answer();

        return $this->answer;
    }

}
