<?php
/*

   Copyright 2018 GILIA

   Author: Giménez, Christian. Braun, Germán

   graphicalrules.php

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Wicom\Translator\Strategies\GOMS;

use \XMLReader;
use \SimpleXMLElement;
use \SimpleXMLIterator;
use \XMLWriter;

/**
  This class contains xpath rules to look for graphical primitives from an owl 2 ontology
*/
class GraphicalRule{

    protected $class = "";
    protected $sub = "";
    protected $sub_total = [];
    protected $sub_partial = [];
    protected $sub_disj = [];
    protected $rel_without_class = [];
    protected $equivalence = [];
    protected $disjointness = [];
    protected $xml = NULL;

    function __construct(){
    }

    function get_sub_rule(){
        return $this->sub;
    }

    function get_subtotal_rule(){
        return $this->sub_total;
    }

    function get_subpartial_rule(){
        return $this->sub_partial;
    }

    function get_subdisj_rule(){
        return $this->sub_disj;
    }

    function get_relwithoutclass_rule(){
        return $this->rel_without_class;
    }

    function get_equivalence_rule(){
        return $this->equivalence;
    }

    function get_disjoint_rule(){
        return $this->disjointness;
    }


}
