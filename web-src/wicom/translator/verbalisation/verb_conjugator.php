<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Wicom\Translator\Verbalisation;

use function \load;

class VerbConjugator {

    /**

      @param json JSON_UML  string
      @return a  string.
     */
    protected $array_verbs;
    protected $conj_verbs;

    function __construct($verbs) {
        $this->array_verbs = $verbs;
        $this->conj_verbs= [];
    }

    function conjugate_verbs() {
        $pattern = "/<p>Pretérito perfecto compuesto<\/p>(\s*)<ul class=\"wrap-verbs-listing\">(\s*)<l\i><\i class=\"graytxt\">\(<\/\i><\i class=\"graytxt\">yo<\/\i><\i class=\"graytxt\">\) <\/\i><\i class=\"auxgraytxt\">he <\/\i><\i class=\"verbtxt\">(.*)<\/\i><\/l\i>/";

        foreach ($this->array_verbs as $verb) {

            $fp = fopen("verbos.txt", "w");

            $url = 'http://conjugador.reverso.net/conjugacion-espanol-verbo-' . $verb . '.html';
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_FILE, $fp);
            curl_exec($curl);
            curl_close($curl);

            $open = file_get_contents("verbos.txt", "r");


            if (preg_match($pattern, $open, $matches)) {
                $palabra = substr($matches[0], 223, -9);
                
                $this->conj_verbs[$verb]=$palabra;
                
            } else {
                echo("No se encontró");
            }

            fclose($fp);
        }
    }
    
    function get_conjugate_verbs(){
        return $this->conj_verbs;
    }

}

