<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Wicom\Translator\Verbalisation;

use function \load;
load("SpGrammarJSONBuilder.php", "../builders/");

use Wicom\Translator\Builders\SpGrammarJSONBuilder;


class PosTagger{
        /**
	
	 @param json JSON_UML  string
	 @return a  string.
	 */
    protected $UML_JSON;
    protected $SpGrammarJSONBuilder;
    protected $verbs;
    
    function __construct($uml_json){
        $this->SpGrammarJSONBuilder=new SpGrammarJSONBuilder();
        $this->UML_JSON= json_decode($uml_json, true);
        $this->verbs=[];
	}
        
    function generate_SpGrammar(){
        $classes=$this->UML_JSON["classes"];        
        foreach($classes as $class){
            $this->SpGrammarJSONBuilder->add_word($class["name"]);
            $attributes=$class["attrs"];
            foreach ($attributes as $attrib){
                $this->SpGrammarJSONBuilder->add_word($attrib["name"]);
            }
        }
    }
    
    function get_grammar_for_links(){
        $js_links=$this->UML_JSON["links"];
        
        $links = array_filter($js_links, function($gen) {
            return $gen["type"] == "association";
        });
        foreach($links as $link){
            array_push($this->verbs,$link["name"]);
        }
        return $this->verbs;
    }
    
    function get_SpGrammar(){
        $product = $this->SpGrammarJSONBuilder->get_product();
        $grammar_json = $product->to_json();
        
        return $grammar_json;
    }
    
}