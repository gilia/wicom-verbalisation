<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Wicom\Translator\Verbalisation;


class Verbalisation{
        /**
	 Verbalise a given FOL String representing a UML class diagram
	
	 @param json JSON_FOL  string
	 @return a Verbalisation string.
	 */
    public $verbalisation;
    public $grammar;
    
    public $verbs;
    
    function __construct(){
        $this->verbalisation= "";
        $this->grammar= [];
        $this->verbs= [];
	}
        
    function generate_verbalisation($fol_str,$grammar_str,$verbs_conj){
        $fol = json_decode($fol_str, true);
        $this->grammar=$grammar_str;
        $this->verbs= $verbs_conj;
        
        //verbalisation of primitives
        $this->verbalisationOfClass($fol);
        $this->verbalisationOfSubsumptions($fol);
        $this->verbalisationOfAssociation($fol);        
    }
    
    function get_verbalisation(){
        return trim($this->verbalisation);
    }
            
    function verbalisationOfClass($fol){         
        $js_classes = $fol["Classes"];
        $js_attributes=$fol["Attribute"];
        
        foreach ($js_classes as $class_pred) {     
            $nameClass=$class_pred["forall"]["pred"]["name"];
            
            $attributes= array_filter($js_attributes,function($attrib) use ($nameClass) {return $attrib["forall"]["imply"]["and"]["pred"]["name"] == $nameClass;});
            
            $this->verbalisation= $this->verbalisation."\n".$this->verbalisationOfClassAndAtributtes($nameClass, $attributes);   
        }
    }
    
    function verbalisationOfSubsumptions($fol){ 
        $js_subsumptions = $fol["IsA"];
         // count of  constrains (==2 is disjoint and covering)
        
         foreach ($js_subsumptions as $key=>$value) {
             $disj_and_cover=0;
             if(array_key_exists("forall", $value)){
                 $this->verbalisationIsA($value);
             }
             else{
                 if(array_key_exists("disjoint", $value)){
                     $this->verbalisationOfDisjointConstraint($value);
                     $disj_and_cover++;
                 }
                 if(array_key_exists("covering",$value)){
                     $disj_and_cover++;
                     $this->verbalisationOfCoveringConstraint($value,$disj_and_cover);
                 }
             }
             
         }
    }
    
    private function verbalisationIsA($subsumption) {
        $parent = $subsumption["forall"]["imply"]["predB"]["name"];
        $child = $subsumption["forall"]["imply"]["pred"]["name"];
        
        $parent_grammar = $this->getGrammarOfWord($parent);
        $child_grammar = $this->getGrammarOfWord($child);

        $subsumption_verb = ucwords($this->article($child_grammar)) . " es " . ucwords($this->article($parent_grammar)).".";

        $this->verbalisation = $this->verbalisation . "\n" . $subsumption_verb;
    }

    private function verbalisationOfDisjointConstraint($constraint) {
        $disjoints_classes = $constraint["disjoint"];

        foreach ($disjoints_classes as $disjoint) {
            $sub1 = $disjoint["forall"]["imply"]["pred"]["name"];
            $sub2 = $disjoint["forall"]["imply"]["neg"]["pred"]["name"];
            
            $grammar_sub1= $this->getGrammarOfWord($sub1);
            $grammar_sub2= $this->getGrammarOfWord($sub2);
            
            if(!$this->isFemale($grammar_sub1)){
                $constraint_verb = "Ningún " . $sub1 . " puede ser " . $this->article($grammar_sub2);
            }
            else{
                $constraint_verb = "Ninguna " . $sub1 . " puede ser " . $this->article($grammar_sub2);
            }
            

            $this->verbalisation = $this->verbalisation . "\n" . $constraint_verb.".";
        }
    }

    private function verbalisationOfCoveringConstraint($constraint,$disj_and_cover){
        $coverings_classes=$constraint["covering"];
        
        foreach($coverings_classes as $covering){
            
            $parent=$covering["forall"]["imply"]["pred"]["name"];
            
            $or_classes=$covering["forall"]["imply"]["or"];
            
            $constraint_verb="Cada ".$parent. " debe ser al menos ";
            
            foreach($or_classes as $or_class){
                $class=$or_class["pred"]["name"];
                $grammar_class = $this->getGrammarOfWord($class);
                
                if($or_class==end($or_classes)){
                    $constraint_verb=$constraint_verb."o ".$this->article($grammar_class);
                }
                else{
                    $constraint_verb=$constraint_verb.$this->article($grammar_class).", ";
                }                                
            }
            
            $this->verbalisation= $this->verbalisation."\n".$constraint_verb;
            
            if($disj_and_cover==2){
                $this->verbalisation= $this->verbalisation.", pero no todos (solo uno de ellos)";
            }
            $this->verbalisation= $this->verbalisation.".";
        }
    }
    
    function verbalisationOfAssociation($fol){
        $js_associations=$fol["Links"];
        foreach($js_associations as $association){ //$association is [3]----> pred1,pred2,pred3
            
            $assoc_name=$association[0]["forall"]["imply"]["pred"]["name"];
            $classA=$association[0]["forall"]["imply"]["and"]["pred"]["name"];
            $classB=$association[0]["forall"]["imply"]["and"]["predB"]["name"];
            
            //next($js_associations);
            
            $multiplicity1=[];
            
            $multiplicity1[0]=$association[1]["forall"]["imply"]["multiplicity"]["min"];
            $multiplicity1[1]=$association[1]["forall"]["imply"]["multiplicity"]["max"];
            $rolA_name=$association[1]["forall"]["imply"]["multiplicity"]["#"]["pred"]["name"];
            
            //next($js_associations);
            
            $multiplicity2=[];
            $multiplicity2[0]=$association[2]["forall"]["imply"]["multiplicity"]["min"];
            $multiplicity2[1]=$association[2]["forall"]["imply"]["multiplicity"]["max"];
            $rolB_name=$association[2]["forall"]["imply"]["multiplicity"]["#"]["pred"]["name"];
            
             $conj_assoc_name= $this->getVerb($assoc_name); //conjugation of verb 
             
           // $verbalisation1=$this->verbalisationOfAssociationWithMultiplicities($classA, $classB, $assoc_name, $rolA_name, $rolB_name, $multiplicity1);
            //$verbalisation2=$this->verbalisationOfAssociationWithMultiplicities($classB, $classA, $assoc_name, $rolA_name, $rolB_name, $multiplicity2);
             
            $verbalisation1=$this->verbalisationOfAssociationWithMultiplicities($classA, $classB, $assoc_name, $rolA_name, $rolB_name, $multiplicity1,0);
            $verbalisation2=$this->verbalisationOfAssociationWithMultiplicities($classB, $classA, "es ".$conj_assoc_name, $rolA_name, $rolB_name, $multiplicity2,1);
            
            $this->verbalisation= $this->verbalisation."\n".$verbalisation1.".\n".$verbalisation2.".";
            
        }
    }
    
    private function verbalisationOfClassAndAtributtes($class_name,$attributes){
        $grammar_class= $this->getGrammarOfWord($class_name);
        $has="tiene ";
        
        if(!empty($attributes)){
            $verbalisation= ucwords($this->article($grammar_class))." ". $has;  //example= "Una persona tiene"  "Un teléfono tiene"
            
            foreach ($attributes as $attribute){      
                $attribute_name= $attribute["forall"]["imply"]["and"]["predA"]["name"];
                $grammar_attribute= $this->getGrammarOfWord($attribute_name);
                
                $aux= $this->article($grammar_attribute);
                
                if($attribute == end($attributes)){
                    if($attribute== reset($attributes)){
                        $verbalisation=$verbalisation. " ". $aux.".";
                    }
                    else{
                        $verbalisation=$verbalisation. "y ". $aux.".";
                    }
                    
                }
                else{
                    $verbalisation=$verbalisation. $aux.", ";
                }
            }
            return $verbalisation;
        }
    }
    
    private function verbalisationOfAssociationWithMultiplicities($classA, $classB, $association, $roleA, $roleB, $mult,$direction){
        $class_gender="Cada ";
        
        if($direction==0){ // normal direction  A -> B
            $verbalisation=$class_gender.$classA." ".$association;
        }
        else{ // inverse direction
            $classA_grammar= $this->getGrammarOfWord($classA);
            if($this->isFemale($classA_grammar)){
                
                $verbalisation=$class_gender.$classA." ".$this->femaleOf($association)." por";
            }
            else{
                $verbalisation=$class_gender.$classA." ".$association." por";
            }
        }
        
        $mult_verbA= $this->verbalizeMultiplicity($mult,$classB);
  
        $verbalisation=$verbalisation." ".$mult_verbA;
        
        return $verbalisation;
    }
    
    private function verbalizeMultiplicity($mult,$classB) {
        $mult_verb = "";
        $classB_grammar= $this->getGrammarOfWord($classB);
        
        if ($mult[0] == "0") {
            if ($mult[1] == "*") {
                $mult_verb = "ninguna o varias";
            } else {
                $mult_verb = "como máximo " . $mult[1]." ". $this->plural($classB_grammar);
            }
        } else { //mult[0] = 1 || ¨=n 
            if (($mult[1] == "1") && ($mult[0] == "1")) {
                $mult_verb = "sólo 1"." ".$classB;
            } else {
                if ($mult[1] == "*") {
                    if($mult[0]=="1"){
                        $mult_verb = "como mínimo 1 " . $classB;
                    }
                    else{
                        $mult_verb = "como mínimo " . $mult[0]." ".$this->plural($classB_grammar);
                    }
                    
                } else {
                    $mult_verb = "como mínimo " . $mult[0] . " y como máximo " . $mult[1]." ".$this->plural($classB_grammar);
                }
            }
        }
        return $mult_verb;
    }

    private function getGrammarOfWord($palabra){
        $words=$this->grammar["words"];
        $word = array_filter($words,function($w) use ($palabra){return $w['word']==$palabra;});   
        //print_r("palabra buscada:     ".$palabra);
        return $word[array_keys($word)[0]];
    }
    
    private function getVerb($name){
        return $this->verbs[$name];
    }
    
    private function isFemale($grammar){
        return $grammar["grammar"]["gender"]=="femenino";
    }
    
    private function article($grammar){ //return --> "Una Persona" / "Un Cliente"
        return $grammar["grammar"]["article"][2];
    }
    
    private function plural($grammar){
        return $grammar["grammar"]["plural"];
    }
    
    private function femaleOf($verb){  //hola   length=4   from 0 to 2
        //input (male verb) => "es estudiado"
        //return "es estudiada"
        $female_verb= substr($verb, 0, strlen($verb)-1);
        $female_verb = $female_verb."a";
        return $female_verb;
    }
    

}