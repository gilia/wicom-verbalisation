<?php 


require_once '../../common/import_functions.php';

load('translator.php', '../../wicom/translator/');
load('owllinkdocument.php', '../../wicom/translator/documents/');
load('crowd_uml.php','../../wicom/translator/strategies/');
load('umlfol.php','../../wicom/translator/fol/');
load('verbalisationUML.php','../../wicom/translator/verbalisation/');
load('postagger.php','../../wicom/translator/verbalisation/');
load('grammarconnector.php','../../wicom/reasoner/');


use Wicom\Translator\Translator;
use Wicom\Translator\Fol\UMLFol;
use Wicom\Translator\verbalisation\Verbalisation;
use Wicom\Translator\Verbalisation\PosTagger;
use Wicom\Reasoner\GrammarConnector;




if ( ! array_key_exists('json', $_POST)){
	echo "
There's no \"json\" parameter :-(
Use, for example:

    curl -d 'json={\"classes\": [{\"attrs\":[], \"methods\":[], \"name\": \"Hi World\"}]}' http://host/translator/metamodel.php";
}else{
    
    
    $uml_fol = new UMLFol();
    $uml_fol->create_fol($_POST['json']);
    $fol = $uml_fol->get_json();
    
    $postagger = new PosTagger($_POST['json']);
    $postagger->generate_SpGrammar();
    $empty_grammar= $postagger->get_SpGrammar();
    
    $crowdgrammar = new GrammarConnector();
    $crowdgrammar->run($empty_grammar);
    $grammar = $crowdgrammar->get_json_grammar();
     
    $strategy = new Verbalisation();
    $strategy->generate_verbalisation($fol,$grammar);
    $res = $strategy->get_verbalisation();
    
    print_r($res);
}

?>
